<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Inventory extends Model
{
    use HasFactory;
    protected $fillable = [
      'tracking_number',
      'first_name',
      'last_name',
      'description',
      'package_type',
      'weight',
      'shipper',
      'house_number',
      'courier',
      'bag_number',
      'barcode',
      'printed',
      'note',
      'type',
      'prefix',
      'created_at',
      'created_by',
    ];
    public function data()
    {
        return $this->hasMany('App\Models\UnknownRequests', 'item_id', 'id');
    }
    public function request($id)
    {
        $requests = UnknownRequests::where('item_id', '=', $id)
        ->count();

        return $requests;
    }
}
