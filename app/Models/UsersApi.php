<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UsersApi extends Model
{
    use HasFactory;
    protected $fillable = [
      'company_name',
      'app_key',
      'created_at',
      'ip_address',
      'status',
    ];
}
