<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Manifest extends Model
{
    use HasFactory;
    protected $fillable = [
        'start_date',
        'end_date',
        'courier',
        'house_number',
        'file_number',
        'weight',
        'packing_list_count',
    ];
}
