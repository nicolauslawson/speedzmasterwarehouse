<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public function container()
    {
        return $this->hasOne('App\Models\Container', 'id', 'container_id');
    }
    public function shipping_val()
    {
        return $this->hasOne('App\Models\ShippingRates', 'id', 'shipping_id');
    }
}