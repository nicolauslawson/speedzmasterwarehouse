<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bag extends Model
{
    use HasFactory;

    protected $fillable = [
        'courier',
        'bag_number',
        'single_piece',
    ];

    public function items()
    {
        return $this->hasMany('App\Models\Inventory', 'bag_number', 'bag_number');
    }

    public function count($id)
    {
        $count = Inventory::where('bag_number', $id)
        ->count();
        
        return $count;
    }

    public function weight($id)
    {
        $weight = Inventory::where('bag_number', $id)
        ->sum('weight');

        return $weight;
    }
}
