<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UnknownRequests extends Model
{
    use HasFactory;
    protected $fillable = [
        'courier',
        'item_id',
        'tracking_number',
        'status',
        'created_at',
    ];
    public function item()
    {
        return $this->hasOne('App\Models\Inventory', 'id', 'item_id');
    }
}
