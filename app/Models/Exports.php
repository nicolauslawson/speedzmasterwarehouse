<?php

namespace App\Http\Controllers;
use DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class ManifestExport extends FromQuery
{
    use Exportable;
    public function query()
    {
        return DB::table('user_types')->get();
    }
}