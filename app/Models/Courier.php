<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Courier extends Model
{
    use HasFactory;

    protected $fillable = [
      'company_name',
      'primary_address',
      'secondary_address',
      'country',
      'city',
      'parish',
      'mobile_cell',
      'mobile_other',
      'abbr',
    ];

    public function api()
    {
        return $this->hasOne('App\Models\UsersApi', 'company_name', 'company_name');
    }
}
