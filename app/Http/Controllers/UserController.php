<?php

namespace App\Http\Controllers;

use DB;
use DataTables;
use App\Models\Manifest;
use App\Models\Inventory;
use App\Models\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UpdatePasswordFormRequest;
use App\Http\Requests\UpdateProfileFormRequest;
use Excel;
use Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function dash() {
        if (Auth::user()->status == 1) {
            if ((Auth::user()->type == 1) || (Auth::user()->type == 2) || (Auth::user()->type == 3) || (Auth::user()->type == 4)) {
                return redirect()->route("admin");
            } else {
                return redirect()->route("dashboard");
            }
        } else {
            return view('errors.block');
        }
    }
    public function dashboard(Inventory $newInvent, Manifest $manifestDetails) {
        if (Auth::user()->status != 1) {
            return view('errors.block');
        }
        
        if (Auth::user()->type == 1 || Auth::user()->type == 2 || Auth::user()->type == 3) {
            return redirect()->route("admin");
        }
        
        $invent = $newInvent->where('courier', Auth::user()->courier)->count();
        $manifest = $manifestDetails->where('courier', Auth::user()->courier)->count();
        $recent = $newInvent->where('courier', Auth::user()->courier)
        ->whereDate('created_at', '=', Carbon::today()->toDateString())
        ->count();
        $items = DB::table('inventories')
            ->select('tracking_number','first_name','last_name','description','weight','house_number','shipper','bag_number','courier','created_at', 'package_type')
            ->where('courier','=', Auth::user()->courier)
            ->whereDate('created_at', '=', Carbon::today()->toDateString())
            ->paginate(50);
        return view('user.index', compact('invent', 'manifest', 'recent', 'items'));
    }
    public function searchItemView() {
        if (Auth::user()->status != 1) {
            return view('errors.block');
        }
        return view('user.searchItem');
    }
    public function userReport(Request $request) {
        if (Auth::user()->status != 1) {
            return view('errors.block');
        }
        $date = Carbon::now()->toDateString();

        if (!$request->date) {
            $date = Carbon::now()->toDateString();
        } else {
            $date = $request->date;
        }
        $items = DB::table('inventories')
            ->select('tracking_number','prefix','first_name','last_name','description','weight','house_number','shipper','bag_number','courier','created_at','package_type', 'id')
            ->where('courier','=', Auth::user()->courier)
            ->whereDate('created_at','=', $date)
            ->paginate(50);
        $weight = DB::table('inventories')->where('courier', '=', Auth::user()->courier)->whereDate('created_at','=', $date)->sum('weight');
        return view('user.report', compact('date', 'items', 'weight'));
    }
    public function allItem(Request $request) {
        if (Auth::user()->status != 1) {
            return view('errors.block');
        }
        $weight = DB::table('inventories')->where('courier', '=', Auth::user()->courier)->sum('weight');
        $count = DB::table('inventories')->where('courier', '=', Auth::user()->courier)->count();
        $tracking = $request->tracking;
        $awb = $request->awb;
        $fn = $request->fn;
        $ln = $request->ln;

        $items = new Inventory;
        $items = $items->select('tracking_number','prefix','first_name','last_name','description','weight','house_number','shipper','bag_number','courier','package_type','created_at');
        if ($tracking) {
            $items = $items->where('tracking_number', 'like', '%' .$tracking. '%');
        }
        if ($awb) {
            $items = $items->where('house_number', $awb);
        }
        if ($fn) {
            $items = $items->where('first_name', 'like', $fn.'%');
        }
        if ($ln) {
            $items = $items->where('last_name', 'like', $ln.'%');
        }
        $items = $items->where('courier', '=', Auth::user()->courier);
        $items = $items->orderBy('created_at', 'desc');
        $items = $items->paginate(100);
        
        return view('user.allItem', compact('items', 'weight', 'count'));
    }
    public function allUnknown() {
        if (Auth::user()->status != 1) {
            return view('errors.block');
        }
        $items = DB::table('inventories')
        ->select('tracking_number','first_name','last_name','description','weight','house_number','shipper','bag_number','courier','created_at','barcode','id')
        ->where('courier', 'Unknown')
        ->where('unknown_status', null)
        ->get();
        return view('user.allUnknown', compact('items'));
    }

    public function profile(Request $request) {
        return view('user.profile');
    }
    public function update(UpdateProfileFormRequest $request, User $user) {
        $updateUser = $user->where('id', Auth::user()->id)->first();
        try {
        $updateUser->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'mobile_cell' => $request->mobile_cell,
            'primary_address' => $request->primary_address,
            'city' => $request->city,
            'parish' => $request->parish,
            // 'email' => $request->email,
        ]);
        }
        catch (QueryException $e) {
        $error_code = $e->errorInfo[1];
        if($error_code == 1062){
            Alert::error('Invalid Email', 'Failed');
            return redirect()->route("profile");
        }
        }
        return redirect()->back()->with('message','Profile update successful');
    }
    public function userManifestGenerateView(Request $request) {
        if (Auth::user()->status != 1) {
            return view('errors.block');
        }
        $courier = Auth::user()->courier;
        if ($request->courier) {
            $check = DB::table('bag_groups')->where('main_courier', Auth::user()->courier)->where('secondary_courier', $request->courier)->first();
            if($check) {
                $courier = $request->courier;
            }
        }
        $start = (Carbon::parse($request->start_date)->format('Y-m-d'));
        $end = (Carbon::parse($request->end_date)->format('Y-m-d'));
        if ($courier == Auth::user()->courier) {
            $items = DB::table('inventories')
                ->select('tracking_number','first_name','last_name','description','weight','house_number','shipper','bag_number','courier','created_at', 'id')
                ->where('courier','=', $courier)
                ->whereBetween('created_at', [$start." 00:00:00", $end." 23:59:59"])
                ->paginate(100);
        } else if ($start >= Carbon::now()->subDays(10)) {
            $items = DB::table('inventories')
                ->select('tracking_number','first_name','last_name','description','weight','house_number','shipper','bag_number','courier','created_at', 'id')
                ->where('courier','=', $courier)
                ->whereBetween('created_at', [$start." 00:00:00", $end." 23:59:59"])
                ->paginate(100);
        }
        return view('user.generateManifest', compact('items'));
    }
    public function updatepassword(UpdatePasswordFormRequest $request, User $user)
    {
        $user = $user->find(Auth::user()->id);
        $oldpassword = Hash::check($request->oldpassword, Auth::user()->password, []);
        if($oldpassword) {
        $user->password = bcrypt($request->password);
        $user->save();
            return redirect()->back()->with('message','Password update successful!');
        } else {
            return redirect()->back()->with('error','Password update fail. Please try again!');
        }
    }
    public function submitUnknown(Request $request) {
        if (Auth::user()->status != 1) {
            return view('errors.block');
        }
        $check = DB::table('unknown_requests')->where('tracking_number', $request->tracking_number)->first();
        if ($check) {
            return redirect()->back()->withErrors('Item already requested!');
        } else {
            DB::table('unknown_requests')->insert([
                'courier' => Auth::user()->courier,
                'item_id' => $request->item_id,
                'tracking_number' => $request->tracking_number,
                'status' => 'Active',
                'created_at' => Carbon::now()
            ]);
            return redirect()->back()->with('success','Item requested successful!');
        }
    }
    public function searchItemDetails(Request $request) {
        if (Auth::user()->status != 1) {
            return view('errors.block');
        }
        $tracking = $request->tracking;
        $fn = $request->fn;
        $ln = $request->ln;
        if(Auth::user()->courier_type == 'air') {
            if ($fn && $ln && $tracking) {
                $newInvent = DB::table('inventories')
                ->where('tracking_number', 'like', '%' .$tracking. '%')
                ->where('first_name', 'like', $request->fn. '%')
                ->where('last_name', 'like', $request->ln. '%')
                ->where('courier', '=', Auth::user()->courier)
                ->orderBy('created_at', 'desc')
                ->limit(200)
                ->get();
            } else if ($fn && $ln) {
                $newInvent = DB::table('inventories')
                ->where('first_name', 'like', $request->fn. '%')
                ->where('last_name', 'like', $request->ln. '%')
                ->where('courier', '=', Auth::user()->courier)
                ->orderBy('created_at', 'desc')
                ->limit(200)
                ->get();
            } else if ($fn && $tracking) {
                $newInvent = DB::table('inventories')
                ->where('tracking_number', 'like', '%' .$tracking. '%')
                ->where('first_name', 'like', $request->fn. '%')
                ->where('courier', '=', Auth::user()->courier)
                ->orderBy('created_at', 'desc')
                ->limit(200)
                ->get();
            } else if ($ln && $tracking) {
                $newInvent = DB::table('inventories')
                ->where('tracking_number', 'like', '%' .$tracking. '%')
                ->where('first_name', 'like', $request->fn. '%')
                ->where('courier', '=', Auth::user()->courier)
                ->orderBy('created_at', 'desc')
                ->limit(200)
                ->get();
            } else if ($fn) {
                $newInvent = DB::table('inventories')
                ->where('first_name', 'like', $request->fn. '%')
                ->where('courier', '=', Auth::user()->courier)
                ->orderBy('created_at', 'desc')
                ->limit(200)
                ->get();
            } else if ($ln) {
                $newInvent = DB::table('inventories')
                ->where('last_name', 'like', $request->ln. '%')
                ->where('courier', '=', Auth::user()->courier)
                ->orderBy('created_at', 'desc')
                ->limit(200)
                ->get();
            } else if ($tracking) {
                $newInvent = DB::table('inventories')
                ->where('tracking_number', 'like', '%' .$tracking. '%')
                ->where('courier', '=', Auth::user()->courier)
                ->orderBy('created_at', 'desc')
                ->limit(200)
                ->get();
            }
        }
        return Datatables::of($newInvent)->make();
    }
    public function userManifestDownload(Request $request, Manifest $manifest) {

        return (new ManifestExport($request->start, $request->end))->download('Manifest.xls');
    }
    public function contact(Request $request) {
        Mail::send('contact.contact', ['name' => $request->name, 'email' => $request->email,  'subject' => $request->subject, 'comment' => $request->message, 'phone' => $request->phone], function ($m) {
            $m->to('nicolaus.lawson@gmail.com', 'Speedzmaster Warehouse')->subject('Contact Form!');
        });
        if (Mail::failures()) {
            return redirect()->back()->with('error','Contact message failed. Try again later');
        } else {
        return redirect()->back()->with('message','Contact message sent');
        }
        return redirect()->back();
    }
}

class ManifestExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct($start, $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    public function headings(): array
    {
        return [
            'Tracking Number',
            'Prefix',
            'First Name',
            'Last Name',
            'Description',
            'Weight',
            'Shipper',
            'House number',
            'Bag Number',
            'Type',
        ];
    }
    public function query()
    {
        return Inventory
            ::query()
            ->select('tracking_number', 'prefix', 'first_name', 'last_name', 'description', 'weight', 'shipper', 'House_number', 'bag_number', 'type')
            ->whereBetween('created_at', [$this->start." 00:00:00", $this->end." 23:59:59"])
            ->where('courier', Auth::user()->courier);
    }
}
class ReportExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct($start, $end, $courier)
    {
        $this->start = $start;
        $this->end = $end;
        $this->courier = $courier;
    }

    public function headings(): array
    {
        return [
            'Tracking Number',
            'Prefix',
            'First Name',
            'Last Name',
            'Description',
            'Weight',
            'Shipper',
            'House number',
            'Bag Number',
            'Type',
        ];
    }
    public function query()
    {
        return Inventory
            ::query()
            ->select('tracking_number', 'prefix', 'first_name', 'last_name', 'description', 'weight', 'shipper', 'House_number', 'bag_number', 'type')
            ->whereBetween('created_at', [$this->start." 00:00:00", $this->end." 23:59:59"])
            ->where('courier', $this->courier);
    }
}
class DailyReportExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct($date, $courier)
    {
        $this->date = $date;
        $this->courier = $courier;
    }

    public function headings(): array
    {
        return [
            'Tracking Number',
            'Prefix',
            'First Name',
            'Last Name',
            'Description',
            'Weight',
            'Shipper',
            'House number',
            'Bag Number',
            'Type',
        ];
    }
    public function query()
    {
        return Inventory
            ::query()
            ->select('tracking_number', 'prefix', 'first_name', 'last_name', 'description', 'weight', 'shipper', 'House_number', 'bag_number', 'type')
            ->whereDate('created_at', $this->date)
            ->where('courier', $this->courier);
    }
}
