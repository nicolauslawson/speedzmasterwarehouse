<?php

namespace App\Http\Controllers;

use DB;
use Carbon\Carbon;
use App\Models\User;
use App\Models\UserStatus;
use App\Models\Manifest;
use App\Models\Invoice;
use App\Models\Container;
use App\Models\ShippingRates;
use App\Models\Courier;
use App\Models\Inventory;
use App\Models\ReceivedItems;
use App\Models\UnknownRequests;
use App\Models\UsersApi;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\Hash;
use App\Models\Bag;
use Excel;
use DataTables;
use GuzzleHttp\Client;
use \Milon\Barcode\DNS1D;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AddCourierFormRequest;
use App\Http\Requests\AddItemFormRequest;
use App\Http\Requests\AddNotificationFormRequest;
use App\Http\Requests\AddUserFormRequest;
use App\Http\Requests\UpdateProfileFormRequest;
use App\Http\Requests\ItemNotification;
use App\Http\Requests\AddCourierGroupRequest;
use App\Http\Requests\GenerateInvoiceFormRequest;
use App\Http\Requests\CreateManifestRequest;

class AdminController extends Controller
{
    public function barcodePrint(Request $request) {
        $customPaper = array(0,0,432,288);
        $data = DB::table('inventories')->where('barcode', 'JRC300001')->orWhere('tracking_number', 'JRC300001')->get()->toArray();
        $pdf = \PDF::loadView('admin.barcode.itemLabel', compact('data', 'barcode'))->setPaper($customPaper, 'landscape');
        return $pdf->stream();
    }
    public function admin(UnknownRequests $unknown) {
        $recent = DB::table('inventories')->whereDate('created_at', Carbon::today())->count();
        $recentSea = DB::table('inventories')->where('shipping_type', 'Sea')->whereDate('created_at', Carbon::today())->count();
        $recentAir = DB::table('inventories')->where('shipping_type', 'Air')->whereDate('created_at', Carbon::today())->count();

        $requests = DB::table('unknown_requests')->where('status', 'active')->count();
        $userRecent = DB::table('inventories')->select('created_by', DB::raw('COUNT(tracking_number) as items'))->whereDate('created_at', Carbon::today())->groupBy('created_by')->get();
        // $unknownRequests = $unknown->where('status', 'active')->get();
        return view('admin.index', compact('recent', 'recentAir', 'recentSea', 'requests', 'userRecent'));
    }
    // item management
    public function addItemView() {
        // $barcode = DNS1D::getBarcodeSVG("4445645656", "C39",1,30);
        $courier = DB::table('couriers')
                    ->orderBy('company_name', 'asc')
                    ->get();
        return view('admin.item.addItem', compact('courier'));
    }
    public function searchItemAdminView(Request $request) {
        if (Auth::user()->status != 1) {
            return view('errors.block');
        }
        $date = date('Y-m-d');

        $data = new Inventory;
        if ($request->tracking_number) {
            $data = $data->where('tracking_number', $request->tracking_number);
        }
        if ($request->first_name) {
            $data = $data->where('first_name', $request->first_name);
        }
        if ($request->last_name) {
            $data = $data->where('last_name', $request->last_name);
        }
        if ($request->date_from) {
            $data = $data->whereDate('created_at', '>=', $request->date_from);
        }
        if ($request->date_to) {
            $data = $data->whereDate('created_at', '<=', $request->date_to);
        }
        $data = $data->orderBy('first_name', 'desc');
        $data = $data->paginate(40);
        return view('admin.item.searchitem', compact('data', 'date'));
    }
    // public function searchItemDetails(Request $request) {
    //     if (Auth::user()->status != 1) {
    //         return view('errors.block');
    //     }
    //     $tracking = $request->tracking;
    //     $fn = $request->fn;
    //     $ln = $request->ln;
    //     if(Auth::user()->courier_type == 'air') {
    //         if ($fn && $ln && $tracking) {
    //             $newInvent = DB::table('inventories')
    //             ->where('tracking_number', 'like', '%' .$tracking. '%')
    //             ->where('first_name', 'like', $request->fn. '%')
    //             ->where('last_name', 'like', $request->ln. '%')
    //             ->orderBy('created_at', 'desc')
    //             ->limit(200)
    //             ->get();
    //         } else if ($fn && $ln) {
    //             $newInvent = DB::table('inventories')
    //             ->where('first_name', 'like', $request->fn. '%')
    //             ->where('last_name', 'like', $request->ln. '%')
    //             ->orderBy('created_at', 'desc')
    //             ->limit(200)
    //             ->get();
    //         } else if ($fn && $tracking) {
    //             $newInvent = DB::table('inventories')
    //             ->where('tracking_number', 'like', '%' .$tracking. '%')
    //             ->where('first_name', 'like', $request->fn. '%')
    //             ->orderBy('created_at', 'desc')
    //             ->limit(200)
    //             ->get();
    //         } else if ($ln && $tracking) {
    //             $newInvent = DB::table('inventories')
    //             ->where('tracking_number', 'like', '%' .$tracking. '%')
    //             ->where('first_name', 'like', $request->fn. '%')
    //             ->orderBy('created_at', 'desc')
    //             ->limit(200)
    //             ->get();
    //         } else if ($fn) {
    //             $newInvent = DB::table('inventories')
    //             ->where('first_name', 'like', $request->fn. '%')
    //             ->orderBy('created_at', 'desc')
    //             ->limit(200)
    //             ->get();
    //         } else if ($ln) {
    //             $newInvent = DB::table('inventories')
    //             ->where('last_name', 'like', $request->ln. '%')
    //             ->orderBy('created_at', 'desc')
    //             ->limit(200)
    //             ->get();
    //         } else if ($tracking) {
    //             $newInvent = DB::table('inventories')
    //             ->where('tracking_number', 'like', '%' .$tracking. '%')
    //             ->orderBy('created_at', 'desc')
    //             ->limit(200)
    //             ->get();
    //         }
    //     }
    //     return Datatables::of($newInvent)->make();
    // }
    public function barcodeView() {
        return view('admin.item.barcode');
    }
    public function adminAllUnknownRequest() {
        return view('admin.item.allUnknownRequest');
    }
    // public function adminAllApproveUnknownRequest() {
    //     return view('admin.item.allApproveUnknownRequest');
    // }
    // public function adminAllDenyUnknownRequest() {
    //     return view('admin.item.allDenyUnknownRequest');
    // }
    // public function downloadApproveUnknown() {
    //     return (new UnknownApproveExport())->download('Approved_unknown.xls');
    // }
    // public function receiveItem() {
    //     return view('admin.item.receiveItem');
    // }
    public function searchuserview() {
        return view('admin.search.search');
    }
    public function resetpassword(Request $request)
    {
        $password = "warehouse#321";
        DB::table('users')
        ->where('id', $request->id)
        ->update(['password' => Hash::make($password)]);
        return redirect()->back()->with('message', 'User password reset!');
    }
    public function viewAllItem() {
        $items = DB::table('inventories')
        ->select('tracking_number','first_name','last_name','description','weight','house_number','shipper','bag_number','courier','barcode','created_at', 'created_by')
        ->where('created_at', '>=', Carbon::now()->subMonth(5))
        ->orderBy('created_at', 'desc')
        ->paginate(300);

        $itemCount = DB::table('inventories')->count();
        $itemCountMonth = DB::table('inventories')->where('created_at', '>=', Carbon::now()->subMonth(5))->count();
        $totalWeight = DB::table('inventories')->sum('weight');

        return view('admin.item.viewAll', compact('items', 'itemCount', 'itemCountMonth', 'totalWeight'));
    }
    // public function viewAllSingle() {
    //     return view('admin.item.viewAllSingle');
    // }
    // public function viewReceiveItem() {
    //     return view('admin.item.viewReceiveItem');
    // }
    public function viewQuery() {
        return view('admin.item.viewQuery');
    }
    public function createBagView() {
        $courier = DB::table('couriers')->get();
        return view('admin.bag.createbag', compact('courier'));
    }
    public function allUnknown() {
        $items = Inventory::select('tracking_number','first_name','last_name','description','weight','house_number','shipper','bag_number','courier','created_at','barcode','id')
        ->where('courier', 'Unknown')
        ->where('unknown_status', null)
        ->paginate(100);
        return view('admin.item.allunknown', compact('items'));
    }
    public function queryItem(Request $request) {
        $courier = DB::table('couriers')->get();
        $item = DB::table('inventories')->where('barcode', $request->barcode)->orWhere('tracking_number', $request->barcode)->first();
        if (!$item) {
            return redirect()->back()->withErrors('Barcode not found!!');
        }
        DB::table('audit_trail')
        ->insert([
                'user' => Auth::user()->first_name,
                'type' => "Query",
                'audit' => $request->barcode,
                'comment' => 'Query item '.$request->barcode.' for '.$item->courier,
                'created_at' => Carbon::now()
        ]);
        return view('admin.item.query', compact('item','courier'));
    }
    // public function queryUnknownItem(Request $request) {
    //     $courier = DB::table('couriers')->get();
    //     if ($request->barcode) {
    //         $item = DB::table('inventories')->where('barcode', $request->barcode)->orWhere('tracking_number', $request->barcode)->orWhere('id', $request->barcode)->first();
    //         $requests = DB::table('unknown_requests')->where('item_id', $item->id)->where('status', 'active')->get();

    //                     DB::table('audit_trail')
    //                     ->insert([
    //                             'user' => Auth::user()->first_name,
    //                             'type' => "Query",
    //                             'audit' => $request->barcode,
    //                             'comment' => 'Unknown Query '.$request->barcode.' for '.$item->courier,
    //                             'created_at' => Carbon::now()
    //                     ]);

    //         if (!$item) {
    //             return redirect()->back()->withErrors('Barcode not found!!');
    //         }
    //     } else {
    //         $item = DB::table('inventories')->where('id', $request->id)->orWhere('tracking_number', $request->id)->first();
    //         $requests = DB::table('unknown_requests')->where('item_id', $item->id)->where('status', 'active')->get();

    //                     DB::table('audit_trail')
    //                     ->insert([
    //                             'user' => Auth::user()->first_name,
    //                             'type' => "Query",
    //                             'audit' => $request->id,
    //                             'comment' => 'Unknown Query '.$request->id.' for '.$item->courier,
    //                             'created_at' => Carbon::now()
    //                     ]);

    //         if (!$item) {
    //             return redirect()->back()->withErrors('Barcode not found!!');
    //         }
    //     }
    //     return view('admin.item.unknownquery', compact('item','courier','requests'));
    // }
    // public function queryUnknownUpdate(Request $request) {
    //     $item = DB::table('inventories')->where('id', $request->id)->first();
    //     $requests = DB::table('unknown_requests')->where('item_id', $item->id)->where('status', 'active')->get();
    //     if (!$item) {
    //         return redirect()->back()->withErrors('Barcode not found!!');
    //     }
    //     DB::table('audit_trail')
    //     ->insert([
    //             'user' => Auth::user()->first_name,
    //             'type' => "Query",
    //             'audit' => $request->barcode,
    //             'comment' => 'Unknown Query '.$request->barcode.' for '.$item->courier,
    //             'created_at' => Carbon::now()
    //     ]);
    //     DB::table('unknown_requests')
    //     ->where('item_id', $item->id)
    //     ->update([
    //             'approve_by' => Auth::user()->first_name,
    //             'status' => $request->status,
    //     ]);
    //     if ($request->status == 'Approve') {
    //         return redirect()->back()->with('message', 'Item Approved!');
    //     } else if ($request->status == 'Deny') {
    //         return redirect()->back()->with('error', 'Item Denied!');
    //     } else if  ($request->status == 'active'){
    //         return redirect()->back()->with('message', 'Item Added Back to Request List!');
    //     } else {
    //         return redirect()->back()->with('error', 'Item Move to Archive List!');
    //     }
    // }
    // public function queryAllUnknownUpdate(Request $request) {
    //     $item = DB::table('inventories')->where('id', $request->id)->first();
    //     if (!$item) {
    //         return redirect()->back()->withErrors('Barcode not found!!');
    //     }
    //     DB::table('audit_trail')
    //     ->insert([
    //             'user' => Auth::user()->first_name,
    //             'type' => "Query",
    //             'audit' => $item->house_number,
    //             'comment' => 'Unknown item deleted',
    //             'created_at' => Carbon::now()
    //     ]);
    //     DB::table('inventories')
    //     ->where('id', $request->id)
    //     ->update([
    //             'unknown_status' => $request->status,
    //     ]);
    //     return redirect()->back()->with('error', 'Item Move to Archive List!');
    // }
    public function reprint(Request $request) {
        $sub = null;
        $customPaper = array(0,0,432,288);
        $data = DB::table('inventories')->where('barcode', $request->barcode)->get()->toArray();
        $pdf = \PDF::loadView('admin.barcode.itemLabel', compact('data'))->setPaper($customPaper, 'landscape');
        return $pdf->stream();
    }
    public function reprintbag(Request $request) {
        $bag = DB::table('bags')->where('bag_number', $request->id)->first();
        $customPaper = array(0,0,432,288);
        $pdf = \PDF::loadView('admin.barcode.bagLabel', compact('bag'))->setPaper($customPaper, 'landscape');
        return $pdf->stream();
    }
    public function queryUpdate(Request $request, Inventory $newInv) {
        if ($request->courier != null) {
            $courier = DB::table('couriers')->where('company_name', $request->courier)->first();
            $latest = DB::table('inventories')->where('courier', $request->courier)
            ->orderBy('created_at', 'DESC')
            ->first();
            if ($latest) {
                $updatebl = preg_replace('/[^0-9]/', '', $latest->house_number);
            } else {
                $updatebl = 10000;
            }
            if ($request->old_courier == "Unknown") {
                DB::table('unknown_requests')->where('item_id', $request->id)->update([
                    'status' => 'Inactive',
                ]);
            }
            if ($request->unknown) {
                DB::table('unknown_requests')->where('item_id', $request->unknown)->update([
                    'status' => 'Inactive',
                ]);
            }
            $code = $courier->abbr.strval((int)$updatebl + 1);
            $hcode = $courier->abbr.strval((int)$updatebl + 1);
            $newInvent = $newInv->find($request->id);
            $newInvent->tracking_number = $request->tracking_number;
            $newInvent->prefix = $request->prefix;
            $newInvent->first_name = $request->first_name;
            $newInvent->last_name = $request->last_name;
            $newInvent->description = $request->description;
            $newInvent->package_type = $request->package_type;
            $newInvent->weight = $request->weight;
            $newInvent->shipper = $request->shipper;
            $newInvent->note = $request->note;
            $newInvent->barcode = $code;
            $newInvent->house_number = $hcode;
            $newInvent->type = $request->type;
            $newInvent->courier = $request->courier;
            $newInvent->created_at = Carbon::now();
            $newInvent->save();
            // $itemDB::table('inventories')->where('id', $request->id)->first();
            return view('admin.item.barcode', compact('newInvent'));
        } else {
            $code = $request->barcode;
            $newInvent = $newInv->find($request->id);
            $newInvent->tracking_number = $request->tracking_number;
            $newInvent->prefix = $request->prefix;
            $newInvent->first_name = $request->first_name;
            $newInvent->last_name = $request->last_name;
            $newInvent->description = $request->description;
            $newInvent->package_type = $request->package_type;
            $newInvent->weight = $request->weight;
            $newInvent->shipper = $request->shipper;
            $newInvent->note = $request->note;
            $newInvent->type = $request->type;
            $newInvent->updated_at = Carbon::now();
            $newInvent->save();
            return redirect()->route('query', ['barcode' => $code]);
        }
    }
    // invoice
    public function createInvoiceView(Request $request) {
        $courier = DB::table('couriers')->get();

        $items = DB::table('inventories')
        ->whereBetween('created_at', [$request->start_date." 00:00:00", $request->end_date." 23:59:59"])
        ->where('courier', $request->courier)
        ->count();

        $teir1 = DB::table('inventories')
        ->whereBetween('created_at', [$request->start_date." 00:00:00", $request->end_date." 23:59:59"])
        ->whereBetween('weight', [0, 10.99])
        ->where('courier', $request->courier)
        ->count();

        $teir2 = DB::table('inventories')
        ->whereBetween('created_at', [$request->start_date." 00:00:00", $request->end_date." 23:59:59"])
        ->whereBetween('weight', [11, 20.99])
        ->where('courier', $request->courier)
        ->count();

        $teir3 = DB::table('inventories')
        ->whereBetween('created_at', [$request->start_date." 00:00:00", $request->end_date." 23:59:59"])
        ->whereBetween('weight', [11, 50.99])
        ->where('courier', $request->courier)
        ->count();

        $teir4 = DB::table('inventories')
        ->whereBetween('created_at', [$request->start_date." 00:00:00", $request->end_date." 23:59:59"])
        ->whereBetween('weight', [51, 100.99])
        ->where('courier', $request->courier)
        ->count();

        $teir5 = DB::table('inventories')
        ->whereBetween('created_at', [$request->start_date." 00:00:00", $request->end_date." 23:59:59"])
        ->whereBetween('weight',[101, 150.99])
        ->where('courier', $request->courier)
        ->count();

        $teir6 = DB::table('inventories')
        ->whereBetween('created_at', [$request->start_date." 00:00:00", $request->end_date." 23:59:59"])
        ->where('weight', ">=", 151)
        ->where('courier', $request->courier)
        ->count();
        
        $total = ($teir1 * $request->teir1) + ($teir2 * $request->teir2) + ($teir3 * $request->teir3) + ($teir4 * $request->teir4) + ($teir5 * $request->teir5) + ($teir6 * $request->teir6) + ($request->bags_seals + $request->handling + $request->delivery_regular);
        return view('admin.invoice.generateInvoice', compact('courier', 'items', 'teir1', 'teir2', 'teir3', 'teir4', 'teir5', 'teir6', 'total'));
    }
    public function createInvoice(GenerateInvoiceFormRequest $request) {
        dd($request);
        return view('admin.invoice.generateInvoice', compact('courier'));
    }
    public function allInvoiceView(Request $request) {
        return view('admin.invoice.allInvoice');
    }
    // user management
    public function billingupdate(Request $request) {
        Courier::where('id', $request->id)
        ->update([
            'tier1' => $request->tier1,
            'tier2' => $request->tier2,
            'tier3' => $request->tier3,
            'tier4' => $request->tier4,
            'tier5' => $request->tier5,
            'tier6' => $request->tier6,
            'bags_cost' => $request->bags_cost,
            'delivery_cost' => $request->delivery_cost,
            'awb_cost' => $request->awb_cost,
        ]);
        return redirect()->back()->with('message', 'Billing details updated successfully!');
    }
    public function addUserView() {
        $userType = DB::table('user_types')->get();
        $courier = DB::table('couriers')->select('company_name')->get();
        return view('admin.user.addUser', compact('userType', 'courier'));
    }
    public function viewAllUser() {
        $user = DB::table('users')
            ->select('first_name','last_name','email','primary_address','city', 'mobile_cell', 'courier', 'id')
            ->paginate(100);
        return view('admin.user.viewAll',compact('user'));
    }
    public function viewSingleUser(Request $request) {
        $user = DB::table('users')->where('id', $request->id)->first();
        $status = DB::table('user_statuses')->where('id', $user->status)->first();
        $userType = DB::table('user_types')->get();
        $currentType = DB::table('user_types')->where('type_id', $user->type)->first();
        return view('admin.user.viewSingleUser', compact('user', 'status', 'userType', 'currentType'));
    }
    public function additem(AddItemFormRequest $request, Inventory $newInvent) {
        $tracking = $request->tracking_number;
        $courier = DB::table('couriers')->where('company_name', $request->courier)->first();
        $latest = DB::table('inventories')->where('courier', $request->courier)
        ->orderBy('created_at', 'DESC')
        ->orderBy('id', 'DESC')
        ->first();
        if ($latest) {
            $updatebl = preg_replace('/[^0-9]/', '', $latest->house_number);
        } else {
            $updatebl = 300000;
        }
        $newInvent->tracking_number = $request->tracking_number;
        $newInvent->first_name = $request->first_name;
        $newInvent->last_name = $request->last_name;
        $newInvent->description = $request->description;
        $newInvent->package_type = $request->package_type;
        $newInvent->weight = $request->weight;
        $newInvent->shipper = $request->shipper;
        $newInvent->prefix = $request->prefix;
        $newInvent->courier = $request->courier;
        $newInvent->note = $request->note;
        $newInvent->type = $request->item_type;
        $newInvent->shipping_type = $request->shipping_type;
        if ($request->item_type == "Single") {
            $newInvent->barcode = $courier->abbr.'SINGLE'.strval((int)$updatebl + 1);
            $newInvent->house_number =  $courier->abbr.'SINGLE'.strval((int)$updatebl + 1);

            DB::table('audit_trail')
            ->insert([
                    'user' => Auth::user()->first_name,
                    'type' => "Add Item",
                    'audit' => $courier->abbr.'SINGLE'.strval((int)$updatebl + 1),
                    'comment' => 'Added item '.$courier->abbr.'SINGLE'.strval((int)$updatebl + 1).' for '.$request->courier,
                    'created_at' => Carbon::now()
            ]);
        } else {
            $newInvent->barcode = $courier->abbr.strval((int)$updatebl + 1);
            $newInvent->house_number =  $courier->abbr.strval((int)$updatebl + 1);

            DB::table('audit_trail')
            ->insert([
                    'user' => Auth::user()->first_name,
                    'type' => "Add Item",
                    'audit' => $courier->abbr.strval((int)$updatebl + 1),
                    'comment' => 'Added item '.$courier->abbr.strval((int)$updatebl + 1).' for '.$request->courier,
                    'created_at' => Carbon::now()
            ]);
        }
        $newInvent->created_by = Auth::user()->first_name;
        $newInvent->created_at = Carbon::now();
        $newInvent->save();

        if($request->item_type == 'Single') {
            DB::table('single_items')
                ->insert([
                    'courier' => $request->courier,
                    'item_barcode' => $courier->abbr.'SINGLE'.strval((int)$updatebl + 1),
                    'type' => 'air',
                    'created_at' => Carbon::now()
            ]);
        }

        if($request->item_type ==  'Single') {
            $sub = $request->sub_courier;
        } else {
            $sub = null;
        }
        $customPaper = array(0,0,432,288);
        $data = DB::table('inventories')->where('barcode', $courier->abbr.strval((int)$updatebl + 1))->get()->toArray();
        $pdf = \PDF::loadView('admin.barcode.itemLabel', compact('data'))->setPaper($customPaper, 'landscape');
        return $pdf->stream();
    }
    public function addUser(AddUserFormRequest $request) {
        $user = new User;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->primary_address = $request->primary_address;
        $user->secondary_address = $request->secondary_address;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->parish = $request->parish;
        $user->mobile_cell = $request->mobile_1;
        $user->mobile_other = $request->mobile_2;
        $user->type = $request->access_level;
        $user->courier = $request->courier;
        $user->courier_type = $request->courier_type;
        $user->password = bcrypt($request->password);
        $user->save();
        DB::table('audit_trail')
        ->insert([
                'user' => Auth::user()->first_name,
                'type' => "Add User",
                'audit' => $request->email,
                'comment' => 'Added user '.$request->email.' for '.$request->courier.' with '.$request->access_level,
                'created_at' => Carbon::now()
        ]);
        return redirect()->route("admin");
    }
    public function updateUser(Request $request) {
        DB::table('users')->where('id', $request->id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'primary_address' => $request->address,
            'city' => $request->city,
            'parish' => $request->parish,
            'country' => $request->country,
            'mobile_cell' => $request->mobile_cell,
            'mobile_other' => $request->mobile_other,
            'type' => $request->access_level,
        ]);
        DB::table('audit_trail')
        ->insert([
                'user' => Auth::user()->first_name,
                'type' => "Update User",
                'audit' => $request->email,
                'comment' => 'Update user '.$request->email,
                'created_at' => Carbon::now()
        ]);
        return redirect()->back()->with('message', 'User updated!');
    }

    public function blockUser(Request $request) {
        if ($request->type == 'block') {
            $user = User::find($request->id);
            $user->status = 2;
            $user->save();

            $api = UsersApi::where('company_name', $user->courier)->first();
            $api->status = 2;
            $api->save();
            return redirect()->back()->with('block', 'User INACTIVE!');
        }
        if ($request->type == 'unblock') {
            $user = User::find($request->id);
            $user->status = 1;
            $user->save();

            $api = UsersApi::where('company_name', $user->courier)->first();
            $api->status = 1;
            $api->save();
            return redirect()->back()->with('unblock', 'User ACTIVE!');
        }
        DB::table('audit_trail')
        ->insert([
                'user' => Auth::user()->first_name,
                'type' => "User Status",
                'audit' => $request->type,
                'comment' => 'User status update: '.$request->type,
                'created_at' => Carbon::now()
        ]);
    }

    public function statusUser(Request $request) {
        $data = DB::table('couriers')->where('id', $request->id)->first();
        if ($data->status == 3) {
            DB::table('couriers')->where('id', $request->id)->update([
                'status' => 1
            ]);
            return redirect()->back()->with('unblock', 'Courier Active!');
        }
        if ($data->status != 3) {
            DB::table('couriers')->where('id', $request->id)->update([
                'status' => 3
            ]);
            return redirect()->back()->with('block', 'Courier Inactive!');
        }
        DB::table('audit_trail')
        ->insert([
                'user' => Auth::user()->first_name,
                'type' => "Courier Status",
                'audit' => $request->type,
                'comment' => 'Courier status update: '.$request->type,
                'created_at' => Carbon::now()
        ]);
    }

    public function apiStatusUpdate(Request $request) {
        $data = Courier::find($request->id);
        UsersApi::where('company_name', $data->company_name)
        ->update(['status' => $request->status]);
        return redirect()->back()->with('message', 'User blocked from using API!');
    }

    // courier management
    public function addCourierView() {
        return view('admin.courier.addcourier');
    }
    public function viewAllCourier() {
        $courier = DB::table('couriers')
            ->select('company_name','abbr','primary_address','city', 'mobile_cell', 'type', 'id')
            ->paginate(100);
        return view('admin.courier.viewAll', compact('courier'));
    }
    public function viewAllCourierGroup() {
        return view('admin.courier.viewAllGroup');
    }
    public function addCourierGroupView() {
        $courier = DB::table('couriers')->select('company_name')->get();
        return view('admin.courier.addgroup', compact('courier'));
    }
    public function addCourierGroup(Request $request) {
        $checkGroup = DB::table('bag_groups')->where('main_courier', $request->main_courier)->where('secondary_courier', $request->secondary_courier)->first();
        if(!$checkGroup) {
            DB::table('bag_groups')
                ->insert([
                    'main_courier' => $request->main_courier,
                    'secondary_courier' => $request->secondary_courier,
                    'created_at' => Carbon::now(),
                    'created_by' => Auth::user()->first_name,
            ]);
        }
        return redirect()->back()->with('message', 'Courier group created!');
    }
    public function courierGroupDelete(Request $request) {
        DB::table('bag_groups')->where('id', $request->id)->delete();
        return redirect()->back()->with('delete', 'Courier group deleted!');
    }
    public function deleteItem(Request $request) {
        DB::table('inventories')->where('house_number', $request->house_number)->delete();
        return redirect()->back()->with('message', 'Item deleted sucessfully!');
    }
    public function generateApiKey(Request $request) {
        $data = Courier::find($request->id);
        UsersApi::updateOrCreate(
            ['company_name' => $data->company_name],
            ['app_key' => Str::random(40)]
        );
        return redirect()->back()->with('message', 'Item deleted sucessfully!');
    }
    public function viewSingleCourier(Request $request) {
        $courier = Courier::where('id', $request->id)->first();
        $status = DB::table('user_statuses')->where('id', $courier->status)->first();
        return view('admin.courier.viewSingleCourier', compact('courier', 'status'));
    }
    public function addCourier(AddCourierFormRequest $request) {
        $type = DB::table('courier_types')->where('type_id', $request->courier_type)->first();
        $newCourier = new Courier;
        $newCourier->company_name = $request->company_name;
        $newCourier->primary_address = $request->primary_address;
        $newCourier->secondary_address = $request->secondary_address;
        $newCourier->country = $request->country;
        $newCourier->city = $request->city;
        $newCourier->parish = $request->parish;
        $newCourier->mobile_cell = $request->mobile_1;
        $newCourier->mobile_other = $request->mobile_2;
        $newCourier->abbr = $request->abbr;
        $newCourier->courier_type = $request->courier_type;
        $newCourier->created_by = Auth::user()->first_name;
        $newCourier->save();
        DB::table('audit_trail')
        ->insert([
                'user' => Auth::user()->first_name,
                'type' => "Courier",
                'audit' => $request->company_name,
                'comment' => 'Courier added:'.$request->company_name,
                'created_at' => Carbon::now()
        ]);
        return redirect()->route("admin");
    }
    public function updateCourier(Request $request) {
        $type = DB::table('courier_types')->where('type_id', $request->type)->first();
        DB::table('couriers')
            ->where('id', $request->id)
            ->update([
                'primary_address' => $request->address,
                'city' => $request->city,
                'parish' => $request->parish,
                'country' => $request->country,
                'mobile_cell' => $request->mobile_cell,
                'mobile_other' => $request->mobile_other,
                'courier_type' => $request->type,
                'updated_by' => Auth::user()->first_name,
        ]);
        DB::table('audit_trail')
        ->insert([
                'user' => Auth::user()->first_name,
                'type' => "Courier",
                'audit' => "Update Courier",
                'comment' => 'Courier updated:'.$request->id,
                'created_at' => Carbon::now()
        ]);
        return redirect()->back()->with('message', 'Courier updated!');
    }

    // // datatables
    // public function userDatatable() {
    //     $user = DB::table('users')
    //         ->select('first_name','last_name','email','primary_address','city', 'mobile_cell', 'courier', 'id')
    //         ->paginate(100);
    //     return Datatables::of($user)->make();
    // }
    // public function courierDatatable() {
    //     $courier = DB::table('couriers')
    //         ->select('id','company_name','abbr','primary_address','city','mobile_cell', 'type')
    //         ->get();
    //     return Datatables::of($courier)->make();
    // }
    // public function courierGroupDatatable() {
    //     $courier = DB::table('bag_groups')
    //         ->select('id','main_courier','secondary_courier','created_at','created_by')
    //         ->get();
    //     return Datatables::of($courier)->make();
    // }
    // public function singleItemDatatable() {
    //     $items = DB::table('inventories')
    //         ->select('tracking_number','first_name','last_name','description','weight','house_number','shipper','bag_number','courier','barcode','created_at')
    //         ->where('type', 'single')
    //         ->get();
    //     return Datatables::of($items)->make();
    // }
    // public function allBagManifestSingleDatatable(Request $request) {
    //     $items = DB::table('inventories')
    //         ->select('tracking_number','first_name','last_name','description','weight','house_number','shipper','bag_number','courier','barcode','created_at')
    //         ->where('manifest_bag', $request->id)
    //         ->get();
    //     return Datatables::of($items)->make();
    // }
    // public function itemsDatatable() {
    //     $items = DB::table('inventories')
    //         ->select('tracking_number','first_name','last_name','description','weight','house_number','shipper','bag_number','courier','barcode','created_at')
    //         ->where('created_at', '>=', Carbon::now()->subMonth())
    //         ->get();
    //     return Datatables::of($items)->make();
    // }
    // public function receivedItemsDatatable() {
    //     $items = DB::table('received_items')
    //         ->whereDate('created_at', Carbon::today())
    //         ->select('tracking_number','uploader', 'created_at')
    //         ->get();
    //     return Datatables::of($items)->make();
    // }
    // public function itemNotificationDatatable() {
    //     $notification = DB::table('item_notification')
    //         ->where('status', 1)
    //         ->select('tracking_number_1','first_name', 'last_name', 'description', 'shipper', 'courier', 'created_at', 'id')
    //         ->get();
    //     return Datatables::of($notification)->make();
    // }
    // public function alertNotification(Request $request) {
    //     $notification = DB::table('item_notification')->where('status', 1)->where('tracking_number_1', $request->tracking_number)->orWhere('tracking_number_2', $request->tracking_number)->orWhere('tracking_number_3', $request->tracking_number)->first();
    //     return view('admin.item.alertNotification', compact('notification'));
    // }
    // public function submitAlertNotification(Request $request) {
    //     DB::table('item_notification')
    //         ->where('id', $request->id)
    //         ->update([
    //             'status' => 0,
    //     ]);
    //     return redirect()->route("admin");
    // }
    public function checkTracking(Request $request) {
    $check = DB::table('inventories')->where('tracking_number', $request->tracking_number)->first();
    $tracking = $request->tracking_number;
    $item_notification = DB::table('item_notification')->where('status', '=', 1)
        ->where(function($query) use ($tracking){
        $query->orWhere('tracking_number_1', '=', $tracking);
        $query->orWhere('tracking_number_2', '=', $tracking);
        $query->orWhere('tracking_number_3', '=', $tracking);
    })->first();
    if ($item_notification != NULL) {
        return response()->json([
            'status' => 'alert',
            'tracking_number' => $request->tracking_number
        ]);
    } else if ($check) {
      return response()->json([
        'status' => 'true',
        'barcode' => $check->barcode,
      ]);
    } else {
      return "False";
    }
  }
  public function billingDetails(Request $request) {
    $check = DB::table('inventories')->where('tracking_number', $request->tracking_number)->first();

    $data = Courier::where('company_name', $request->courier)->first();
    if ($data) {
      return response()->json([
        'status' => 'true',
        'data' => $data,
      ]);
    } else {
      return "False";
    }
  }
  //manifest
    // public function allManifest() {
    //     return view('admin.manifest.allManifest');
    // }
    // public function viewManifest() {
    //     return view('admin.manifest.viewSingleManifest');
    // }
    public function createManifestView(Request $request) {
        $itemCount = null;
        $totalWeight = null;
        $courier = DB::table('couriers')
                    ->orderBy('company_name', 'asc')
                    ->get();

        $type = $request->type;
        $data = new Inventory;
        $data = $data->where('shipping_type', $request->type);
        $data = $data->orderBy('created_at', 'desc');
        if ($request->has('courier')) {
            $data = $data->where('courier', $request->courier);
        }
        if ($request->has('start_date') && $request->has('end_date')) {
            $data = $data->whereDate('created_at','>=', $request->start_date);
            $data = $data->whereDate('created_at','<=', $request->end_date);
        }
        $itemCount = $data->count();
        $totalWeight = $data->sum('weight');
        $data = $data->paginate(100);

        return view('admin.manifest.createManifest', compact('courier', 'itemCount', 'totalWeight', 'data', 'type'));
    }

    public function downloadManifest(Request $request) {
        $start = (Carbon::parse($request->start_date)->format('Y-m-d'));
        $end = (Carbon::parse($request->end_date)->format('Y-m-d'));
        return (new ManifestExport($start, $end, $request->courier))->download('FL-Manifest.xls');
    }
    
//   bag
    public function packBagView(Request $request) {
        return view('admin.bag.packbagview');
    }
    public function checkBagView(Request $request) {
        return view('admin.bag.checkbagview');
    }
    public function allBagView(Request $request) {
        $date = Carbon::now()->toDateString();
        if ($request->courier) {

            $courier = DB::table('couriers')
            ->orderBy('company_name', 'asc')
            ->get();

            $data = DB::table('couriers')->select('company_name')->where('id', $request->courier)->first();
            $bag = Bag::orderBy('created_at', 'desc')->where('courier', $data->company_name)->whereBetween('created_at', [$request->sd." 00:00:00", $request->ed." 23:59:59"])->paginate(100);

            $weightAll = Bag::orderBy('created_at', 'desc')
            ->join('inventories', 'inventories.bag_number', '=', 'bags.bag_number')
            ->where('bags.courier', $data->company_name)
            ->whereBetween('bags.created_at', [$request->sd." 00:00:00", $request->ed." 23:59:59"])
            ->sum('weight');
            $weight = number_format($weightAll, 2);
            
        } else {
            $courier = DB::table('couriers')
            ->orderBy('company_name', 'asc')
            ->get();
            $bag = Bag::orderBy('created_at', 'desc')->paginate(70);
            $weightAll = Bag::orderBy('created_at', 'desc')
            ->join('inventories', 'inventories.bag_number', '=', 'bags.bag_number')
            ->sum('weight');
            $weight = number_format($weightAll, 2);
        }
        return view('admin.bag.allbag', compact('bag', 'courier', 'date', 'weight'));
    }
    public function checkBag(Request $request) {
        $item = DB::table('bags')->where('bag_number', $request->bag_code)->first();
        if (!$item) {
            return redirect()->back()->withErrors('Bag not found!!');
        }
        $bag = DB::table('inventories')->where('bag_number', '=', $request->bag_code)->get();
        $count = DB::table('inventories')->where('bag_number', '=', $request->bag_code)->count();
        $weight = DB::table('inventories')->where('bag_number', '=', $request->bag_code)->sum('weight');
        return view('admin.bag.checkbag', compact('item', 'bag', 'count', 'weight'));
    }
    public function packBagSubmit(Request $request) {
        $item = DB::table('bags')->where('bag_number', $request->bag_code)->first();
        if (!$item) {
            return redirect()->back()->withErrors('Bag not found!!');
        }
        $bag = DB::table('inventories')->where('bag_number', '=', $request->bag_code)->orderBy('updated_at', 'desc')->get();
        $weight = DB::table('inventories')->where('bag_number', '=', $request->bag_code)->sum('weight');
        return view('admin.bag.packbagsubmit', compact('item', 'bag', 'weight'));
    }
    public function userInvoice(Request $request) {
        $containers = DB::table('containers')->get();
        $shipping = DB::table('shipping_rates')->get();
        return view('admin.invoice.userInvoice', compact('containers', 'shipping'));
    }
    public function userInvoiceSubmit(Request $request) {
        $id = Invoice::insertGetId([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'container_id' => $request->container,
                    'container_quantity' => $request->quantity,
                    'shipping_id' => $request->shipping_type,
                    'shipping_quantity' => $request->shipping_quantity,
                    'freight' => $request->freight,
                    'shipping' => $request->shipping,
                    'total' => 0,
                    'created_at' => Carbon::now(),
                    'created_by' => Auth::user()->first_name
            ]);
        $data = Invoice::find($id);

        $val1 = $data->container ? $data->container->price * $request->quantity : 0;
        $val2 = $data->shipping_val ? $data->shipping_val->price * $request->shipping_quantity : 0;
        $subtotal = $val1 + $val2;
        $total = $subtotal + $data->freight + $data->shipping; 
        $pdf = \PDF::loadView('admin.invoice.invoice', compact('data', 'subtotal', 'total'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
    public function packDelete(Request $request) {
        $itemData = DB::table('inventories')->where('house_number', $request->id)->first();
        DB::table('inventories')
        ->where('house_number', $request->id)
        ->update(['bag_number' => null]);
        DB::table('audit_trail')
        ->insert([
                'user' => Auth::user()->first_name,
                'type' => "Bag",
                'audit' => $itemData->id,
                'comment' => 'Item deleted from bag:'.$itemData->bag_number.' Item #:'.$request->id,
                'created_at' => Carbon::now()
        ]);
        return redirect()->back()->with('delete', 'Item removed from bag!');
    }
    public function packBag(Request $request) {
        $checkItem = DB::table('inventories')->where('barcode', $request->item_barcode)->orWhere('tracking_number', $request->item_barcode)->first();
        if ($checkItem == null) {
            return redirect()->back()->with('exist', 'Item doesnt exist!!');
        }
        $checkBag = DB::table('bags')->where('bag_number', $request->bag_number)->first();
        $group = DB::table('bag_groups')->where('main_courier', $checkBag->courier)->where('secondary_courier', $checkItem->courier)->first();
        if ($checkBag->courier === $checkItem->courier) {
            DB::table('inventories')
            ->where('barcode', $request->item_barcode)
            ->orWhere('tracking_number', $request->item_barcode)
            ->update(['bag_number' => $request->bag_number, 'updated_at' => Carbon::now()]);
            DB::table('audit_trail')
            ->insert([
                    'user' => Auth::user()->first_name,
                    'type' => "Bag",
                    'audit' => $checkItem->house_number,
                    'comment' => 'Item packed into bag: '.$request->bag_number,
                    'created_at' => Carbon::now()
            ]);
            return redirect()->back();
        } else if ($group != null){
            DB::table('inventories')
            ->where('barcode', $request->item_barcode)
            ->orWhere('tracking_number', $request->item_barcode)
            ->update(['bag_number' => $request->bag_number, 'updated_at' => Carbon::now()]);
            DB::table('audit_trail')
            ->insert([
                    'user' => Auth::user()->first_name,
                    'type' => "Bag",
                    'audit' => $checkItem->house_number,
                    'comment' => 'Item packed into bag: '.$request->bag_number,
                    'created_at' => Carbon::now()
            ]);
            return redirect()->back();
        } else {
            DB::table('audit_trail')
            ->insert([
                    'user' => Auth::user()->first_name,
                    'type' => "Bag",
                    'audit' => $checkItem->house_number,
                    'comment' => 'Item trying to go in wrong bag: '.$request->bag_number,
                    'created_at' => Carbon::now()
            ]);
            return redirect()->back()->withErrors('Item doesnt belong to this courier!! Contact system admin');
        }
    }
    public function createBag(Request $request, Bag $bag, Courier $cour) {
        $courier = $cour->where('company_name', $request->courier)->first();
        $latest = $bag->where('courier', $request->courier)
        ->orderBy('id', 'desc')
        ->latest()->first();
        if ($latest == null) {
        $updatebg = 50000;
        } else {
        $updatebg = intval($latest->bag_code) + 1;
        }
        $appr = "BG";
        if ($request->type == "Single") {
        $appr="SIN";
        DB::table('inventories')
        ->where('barcode', $request->item)
        ->update(['bag_number' => 'Single Item']);
        }
        $bag->courier = $request->courier;
        $bag->bag_number = $courier->abbr. $appr .strval($updatebg);
        $bag->bag_code = $updatebg;
        $bag->single_piece = $request->single;
        $bag->item_barcode = $request->item;
        $bag->save();
        DB::table('audit_trail')
        ->insert([
                'user' => Auth::user()->first_name,
                'type' => "Bag",
                'audit' => $courier->abbr. $appr .strval($updatebg),
                'comment' => 'Bag created:'.$courier->abbr. $appr .strval($updatebg),
                'created_at' => Carbon::now()
        ]);
        $customPaper = array(0,0,432,288);
        $pdf = \PDF::loadView('admin.barcode.bagLabel', compact('bag'))->setPaper($customPaper, 'landscape');
        return $pdf->stream();
        // return view('admin.bag.barcodebag', compact('bag'));
    }
    //audit
    public function packageaudit(Request $request) {
        $data = DB::table('inventories')->select('created_by', DB::raw('COUNT(tracking_number) as items'))->whereDate('created_at', Carbon::today())->groupBy('created_by')->paginate(20);
        return view('admin.audit.package', compact('data'));
    }
}

class ManifestExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct($start_date, $end_date, $courier)
    {
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->courier = $courier;
    }

    public function headings(): array
    {
        return [
            'Tracking Number',
            'First Name',
            'Last Name',
            'Weight',
            'Description',
            'Bag Number',
            'House Number',
            'Shipper',
            'Type',
            'Package Type',
        ];
    }
    public function query()
    {
        return Inventory
            ::query()
            ->select('tracking_number', 'first_name', 'last_name', 'weight', 'description', 'bag_number', 'house_number', 'shipper', 'type', 'package_type')
            ->whereBetween('created_at', [$this->start_date." 00:00:00", $this->end_date." 23:59:59"])
            ->where('courier', $this->courier);
    }
}
class ManifestBagExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct($manifest_bag)
    {
        $this->manifest_bag = $manifest_bag;
    }

    public function headings(): array
    {
        return [
            'Tracking Number',
            'First Name',
            'Last Name',
            'Weight',
            'Description',
            'Bag Number',
            'House Number',
            'Shipper',
            'Package Type',
        ];
    }
    public function query()
    {
        return Inventory
            ::query()
            ->select('tracking_number','first_name','last_name','weight','description','bag_number','house_number','shipper','type','package_type')
            ->where('manifest_bag', $this->manifest_bag);
    }
}
class UnknownExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct()
    {
    }

    public function headings(): array
    {
        return [
            'Tracking Number',
            'Prefix',
            'First Name',
            'Last Name',
            'Description',
            'Weight',
            'Shipper',
            'House number',
            'Bag Number',
            'Type',
        ];
    }
    public function query()
    {
        return Inventory
            ::query()
            ->select('tracking_number', 'prefix', 'first_name', 'last_name', 'description', 'weight', 'shipper', 'House_number', 'bag_number', 'type')
            ->where('courier', 'Unknown')
            ->where('unknown_status', null);
    }
}
class UnknownApproveExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct()
    {
    }

    public function headings(): array
    {
        return [
            'Courier',
            'First Name',
            'Last Name',
            'Tracking #',
            'House Number',
            'Weight',
            'Description',
        ];
    }
    public function query()
    {
        return UnknownRequests
            ::query()
            ->select('unknown_requests.courier', 'inventories.first_name', 'inventories.last_name','inventories.tracking_number as tracking', 'inventories.house_number', 'inventories.weight', 'inventories.description')
            ->join('inventories', 'inventories.id', '=', 'unknown_requests.item_id')
            ->where('unknown_requests.status', 'approve')
            ->where('inventories.courier', 'Unknown');
    }
}