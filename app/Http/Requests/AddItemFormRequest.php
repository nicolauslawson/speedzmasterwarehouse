<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class AddItemFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tracking_number' => 'required|string',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'package_type' => 'required|string',
            'description' => 'required|string',
            'weight' => 'required|numeric|max:7000',
            'item_type' => 'required|string',
            'shipper' => 'required|string',
            'prefix' => 'nullable|string',
            'note' => 'nullable|string',
            'courier' => 'required',
        ];
    }
}
