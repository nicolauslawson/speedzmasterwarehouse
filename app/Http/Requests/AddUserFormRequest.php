<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AddUserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email',
            'primary_address' => 'required|string',
            'secondary_address' => 'nullable|string',
            'country' => 'required|string',
            'city' => 'required|string',
            'parish' => 'required|string',
            'mobile_1' => 'required|string',
            'mobile_2' => 'nullable|string',
            'courier' => 'required|string',
            'access_level' => 'required|numeric',
            'password' => 'required|string|confirmed',
        ];
    }
}
