<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AddCourierFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|string',
            'primary_address' => 'required|string',
            'secondary_address' => 'nullable|string',
            'country' => 'required|string',
            'city' => 'required|string',
            'parish' => 'required|string',
            'mobile_1' => 'required|string',
            'mobile_2' => 'nullable|string',
            'abbr' => 'required|string|max:5',
        ];
    }
}
