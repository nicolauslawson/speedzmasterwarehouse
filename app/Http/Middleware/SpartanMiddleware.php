<?php

namespace App\Http\Middleware;

use Closure;

class SpartanMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->type == 3)
        {
          return $next($request);
        } else if ($request->user()->type == 2) {
          return $next($request);
        } else if ($request->user()->type == 1) {
          return $next($request);
        } else {
          return redirect('error');
        }
    }
}
