<?php

namespace App\Http\Middleware;
use DB;

use Closure;

class AuthKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('APPKEY');
        $checkToken = DB::table('users_apis')->where('app_key', $token)->where('status', 1)->first();
        if($checkToken) {
            return $next($request);
        }
        return response()->json(['message' => 'User Not Found'], 401);
    }
}
