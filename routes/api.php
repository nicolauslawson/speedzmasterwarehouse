<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

    
    
Route::group(['prefix' => '','middleware' => 'App\Http\Middleware\AuthKey'], function() {
    Route::get('/item/recent', function (Request $request) {
        $token = $request->header('APPKEY');
        $time = $request->header('TIME');
        if ($time) {
            $user = DB::table('users_apis')->where('app_key', $token)->first();
            $items = DB::table('inventories')
                ->select('tracking_number','prefix', 'first_name', 'last_name', 'description', 'weight', 'shipper', 'house_number', 'type', 'created_at')
                ->where('courier', $user->company_name)
                ->where('created_at', '>', $time)
                ->get();
            return response()->json($items);
        } else {
            $user = DB::table('users_apis')->where('app_key', $token)->first();
            $last_execution = DB::table('execution_times')->select('execution_time')->where('courier', $user->company_name)->first();
            $items = DB::table('inventories')
                ->select('tracking_number','prefix', 'first_name', 'last_name', 'description', 'weight', 'shipper', 'house_number', 'type', 'created_at')
                ->where('courier', $user->company_name)
                ->where('created_at', '>=', $last_execution->execution_time)
                ->get();
            DB::table('execution_times')
                ->where('courier', $user->company_name)
                ->update([
                'execution_time' => Carbon::now(),
                'user' => 'API',
                'ip_address' => $request->ip(),
                'created_at' => Carbon::now(),
            ]);
            return response()->json($items);
        }
    });
});
