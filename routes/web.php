<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
});
Route::post('/contact', [
  'as' => 'contact',
  'uses' => 'UserController@contact',
]);
Route::get('/dash', [
    'middleware' => 'auth',
    'as' => 'dash',
    'uses' => 'UserController@dash',
]);
Route::get('/denied', function () {
    return view('errors.block');
});

Route::group(['prefix' => 'admin','middleware' => 'App\Http\Middleware\AdminMiddleware'], function () {
    Route::get('/audit/package', [App\Http\Controllers\AdminController::class, 'packageaudit'])->name('packageaudit');
    Route::get('/reset/password', [App\Http\Controllers\AdminController::class, 'resetpassword'])->name('resetpassword');
    Route::post('/generate-key', [App\Http\Controllers\AdminController::class, 'generateApiKey'])->name('generateApiKey');
    Route::get('/api/status', [App\Http\Controllers\AdminController::class, 'apiStatusUpdate'])->name('apiStatusUpdate');
    Route::post('/billing/update', [App\Http\Controllers\AdminController::class, 'billingupdate'])->name('billingupdate');
    Route::get('/barcode/print', [App\Http\Controllers\AdminController::class, 'barcodePrint'])->name('barcodePrint');
    Route::get('/add/user', [App\Http\Controllers\AdminController::class, 'addUserView'])->name('addUserView');
    Route::get('/query/delete', [App\Http\Controllers\AdminController::class, 'deleteItem'])->name('deleteItem');
    Route::post('/add/user', [App\Http\Controllers\AdminController::class, 'addUser'])->name('addUser');
    Route::post('/update/user', [App\Http\Controllers\AdminController::class, 'updateUser'])->name('updateUser');
    Route::get('/view/user', [App\Http\Controllers\AdminController::class, 'viewSingleUser'])->name('viewSingleUser');
    Route::post('/block/user', [App\Http\Controllers\AdminController::class, 'blockUser'])->name('blockUser');
    Route::get('/status/user', [App\Http\Controllers\AdminController::class, 'statusUser'])->name('statusUser');
    Route::post('/delete/user', [App\Http\Controllers\AdminController::class, 'deleteUser'])->name('deleteUser');
    Route::get('/add/courier', [App\Http\Controllers\AdminController::class, 'addCourierView'])->name('addCourier');
    Route::post('/add/courier', [App\Http\Controllers\AdminController::class, 'addCourier'])->name('addCourier');
    Route::post('/update/courier', [App\Http\Controllers\AdminController::class, 'updateCourier'])->name('updateCourier');
    Route::get('/add/courier/group', [App\Http\Controllers\AdminController::class, 'addCourierGroupView'])->name('addCourierGroup');
    Route::post('/add/courier/group', [App\Http\Controllers\AdminController::class, 'addCourierGroup'])->name('addCourierGroup');
    Route::get('/view/courier', [App\Http\Controllers\AdminController::class, 'viewSingleCourier'])->name('viewSingleCourier');
    Route::get('/view/all/user', [App\Http\Controllers\AdminController::class, 'viewAllUser'])->name('viewAllUser');
    Route::get('/view/all/courier', [App\Http\Controllers\AdminController::class, 'viewAllCourier'])->name('viewAllCourier');
    Route::get('/view/all/courier/group', [App\Http\Controllers\AdminController::class, 'viewAllCourierGroup'])->name('viewAllCourierGroup');
    Route::get('/courier/group/delete', [App\Http\Controllers\AdminController::class, 'courierGroupDelete'])->name('courierGroupDelete');
    Route::get('/user/search', [App\Http\Controllers\AdminController::class, 'searchuserview'])->name('searchuser');
    Route::post('/user/search', [App\Http\Controllers\AdminController::class, 'searchuser'])->name('searchuser');
    Route::get('/view/all', [App\Http\Controllers\AdminController::class, 'viewAllItem'])->name('viewAllItem');
});

Route::group(['prefix' => '','middleware' => 'App\Http\Middleware\SpartanMiddleware'], function () {
    Route::get('/admin', [App\Http\Controllers\AdminController::class, 'admin'])->name('admin');
    Route::get('/admin/search/item', [App\Http\Controllers\AdminController::class, 'searchItemAdminView'])->name('searchItemAdminView');
    Route::get('/admin/search/item/details', [App\Http\Controllers\AdminController::class, 'searchItemDetails'])->name('searchItemDetailsAdmin');
    Route::get('/admin/all/unknown', [App\Http\Controllers\AdminController::class, 'allUnknown'])->name('adminAllUnknown');
    Route::get('/admin/all/unknown/request', [App\Http\Controllers\AdminController::class, 'adminAllUnknownRequest'])->name('adminAllUnknownRequest');
    Route::get('/admin/all/unknown/approve', [App\Http\Controllers\AdminController::class, 'adminAllApproveUnknownRequest'])->name('adminAllApproveUnknownRequest');
    Route::get('/admin/all/unknown/deny', [App\Http\Controllers\AdminController::class, 'adminAllDenyUnknownRequest'])->name('adminAllDenyUnknownRequest');
    Route::get('/admin/approve/unknown/download', [App\Http\Controllers\AdminController::class, 'downloadApproveUnknown'])->name('downloadApproveUnknown');
    Route::get('/admin/unknown/datatable', [App\Http\Controllers\AdminController::class, 'unknownDatatable'])->name('unknownDatatable')->name('unknownDatatable');
    Route::get('/admin/unknown/request/datatable', [App\Http\Controllers\AdminController::class, 'unknownrequestDatatable'])->name('unknownrequestDatatable');
    Route::get('/admin/unknown/approve/datatable', [App\Http\Controllers\AdminController::class, 'unknownapproverequestDatatable'])->name('unknownapproverequestDatatable');
    Route::get('/admin/unknown/deny/datatable', [App\Http\Controllers\AdminController::class, 'unknowndenyrequestDatatable'])->name('unknowndenyrequestDatatable');
    Route::get('/add/item', [App\Http\Controllers\AdminController::class, 'addItemView'])->name('addItem')->name('addItem');
    Route::post('/add/item', [App\Http\Controllers\AdminController::class, 'addItem'])->name('addItem');
    Route::get('/query/view', [App\Http\Controllers\AdminController::class, 'viewQuery'])->name('viewQuery');
    Route::get('/query', [App\Http\Controllers\AdminController::class, 'queryItem'])->name('query');
    Route::post('/query', [App\Http\Controllers\AdminController::class, 'queryUpdate']);
    Route::get('/reprint', [App\Http\Controllers\AdminController::class, 'reprint'])->name('reprint');
    Route::get('/reprint/bag', [App\Http\Controllers\AdminController::class, 'reprintbag'])->name('reprintbag');
    Route::get('/query/unknown', [App\Http\Controllers\AdminController::class, 'queryUnknownItem']);
    Route::get('/query/unknown/update', [App\Http\Controllers\AdminController::class, 'queryUnknownUpdate']);
    Route::get('/query/allunknown/update', [App\Http\Controllers\AdminController::class, 'queryAllUnknownUpdate']);
    Route::get('/received/item', [App\Http\Controllers\AdminController::class, 'receiveItem']);
    Route::post('/received/item', [App\Http\Controllers\AdminController::class, 'addreceive']);
    Route::get('/item/notification', [App\Http\Controllers\AdminController::class, 'itemNotificationView'])->name('itemNotification');
    Route::get('/unknown/download', [App\Http\Controllers\AdminController::class, 'unknownDownload']);
    Route::get('/item/notification/delete', [App\Http\Controllers\AdminController::class, 'deleteItemNotification']);
    Route::post('/item/notification', [App\Http\Controllers\AdminController::class, 'itemNotification']);
    Route::get('/view/item/notification', [App\Http\Controllers\AdminController::class, 'viewSingleItemNotification']);
    Route::post('/view/item/notification', [App\Http\Controllers\AdminController::class, 'updateItemNotification']);
    Route::get('/view/all/notification', [App\Http\Controllers\AdminController::class, 'viewAllNotification']);
    Route::get('/item/notification/alert', [App\Http\Controllers\AdminController::class, 'alertNotification']);
    Route::post('/item/notification/alert', [App\Http\Controllers\AdminController::class, 'submitAlertNotification']);
    Route::get('/item/single', [App\Http\Controllers\AdminController::class, 'viewAllSingle']);
    Route::get('/tracking/check', [App\Http\Controllers\AdminController::class, 'checkTracking']);
    Route::get('/billing/details', [App\Http\Controllers\AdminController::class, 'billingDetails']);
    // Route::get('/', [App\Http\Controllers\AdminController::class, '']);

    Route::prefix('bag')->group(function () {
        Route::get('/create', [App\Http\Controllers\AdminController::class,'createBagView'])->name('createBag');
        Route::post('/create', [App\Http\Controllers\AdminController::class,'createBag']);
        Route::get('/create/label', [App\Http\Controllers\AdminController::class,'createBagLabelView'])->name('createBagLabel');
        Route::get('/pack', [App\Http\Controllers\AdminController::class,'packBagView'])->name('packBagView');
        Route::get('/pack/id', [App\Http\Controllers\AdminController::class,'packBagSubmit'])->name('packBagSubmit');
        Route::post('/pack', [App\Http\Controllers\AdminController::class,'packBag'])->name('packBagView');
        Route::get('/pack/datatable', [App\Http\Controllers\AdminController::class,'packBagDatatable'])->name('packBagDatatable');
        Route::get('/pack/delete', [App\Http\Controllers\AdminController::class,'packDelete'])->name('packDelete');
        Route::get('/check', [App\Http\Controllers\AdminController::class,'checkBagView'])->name('checkBagView');
        Route::get('/check/id', [App\Http\Controllers\AdminController::class,'checkBag'])->name('checkBag');
        Route::get('/check/datatable/{id}', [App\Http\Controllers\AdminController::class,'checkBagDatatable'])->name('checkBagDatatable');
        Route::get('/all', [App\Http\Controllers\AdminController::class,'allBagView'])->name('allBagView');
        Route::get('/all/datatable', [App\Http\Controllers\AdminController::class,'allBagDatatable'])->name('allBagDatatable');
    });

    Route::prefix('manifest')->group(function () {
        Route::get('/create', [App\Http\Controllers\AdminController::class,'createManifestView'])->name('createManifestView');
        Route::post('/create', [App\Http\Controllers\AdminController::class,'createManifest']);
        Route::get('/bag/view', [App\Http\Controllers\AdminController::class,'viewManifestBagView'])->name('viewManifestBagView');
        Route::get('/bag/single', [App\Http\Controllers\AdminController::class,'viewManifestBagSingle'])->name('viewManifestBagSingle');
        Route::post('/download', [App\Http\Controllers\AdminController::class,'downloadManifest'])->name('downloadManifest');
        Route::post('/bag/download', [App\Http\Controllers\AdminController::class,'downloadBagManifest'])->name('downloadBagManifest');
        Route::get('/delete', [App\Http\Controllers\AdminController::class,'deleteManifest'])->name('deleteManifest');
        Route::get('/all', [App\Http\Controllers\AdminController::class,'allManifest'])->name('allManifest');
        Route::get('/datatable', [App\Http\Controllers\AdminController::class,'allManifestDatatable'])->name('allManifestDatatable');
        Route::get('/bag/datatable', [App\Http\Controllers\AdminController::class,'allBagManifestDatatable'])->name('allBagManifestDatatable');
        Route::get('/bag/single/datatable', [App\Http\Controllers\AdminController::class,'allBagManifestSingleDatatable'])->name('allBagManifestSingleDatatable');
        Route::get('/view', [App\Http\Controllers\AdminController::class,'viewManifest'])->name('viewManifest');
        Route::get('/view/{courier}/{start_date}/{end_date}', [App\Http\Controllers\AdminController::class,'viewManifestDataTable'])->name('viewManifest');
    });

    Route::prefix('manifest')->group(function () {
        Route::get('/create', [App\Http\Controllers\AdminController::class,'createManifestView'])->name('createManifestView');
        Route::put('/create', [App\Http\Controllers\AdminController::class,'createManifest']);
        Route::get('/create/bag', [App\Http\Controllers\AdminController::class,'createManifestBagView'])->name('createManifestBagView');
        Route::get('/create/single', [App\Http\Controllers\AdminController::class,'createManifestSingleView'])->name('createManifestSingleView');
        Route::get('/bag/add', [App\Http\Controllers\AdminController::class,'addManifestBag'])->name('addManifestBag');
        Route::get('/single/add', [App\Http\Controllers\AdminController::class,'addManifestSingle'])->name('addManifestSingle');
        Route::get('/all', [App\Http\Controllers\AdminController::class,'allManifest'])->name('allManifest');
        Route::get('/datatable', [App\Http\Controllers\AdminController::class,'allManifestDatatable'])->name('allManifestDatatable');
        Route::get('/view', [App\Http\Controllers\AdminController::class,'viewManifest'])->name('viewManifest');
        Route::get('/view/{courier}/{start_date}/{end_date}', [App\Http\Controllers\AdminController::class,'viewManifestDataTable'])->name('viewManifest');
        Route::get('/view/single/{courier}/{start_date}/{end_date}', [App\Http\Controllers\AdminController::class,'viewManifestSingleDataTable'])->name('viewManifestSingle');
        Route::get('/view/bag/{courier}/{start_date}/{end_date}', [App\Http\Controllers\AdminController::class,'viewManifestBagDataTable'])->name('viewManifestBag');
    });

    Route::prefix('invoice')->group(function () {
        Route::get('/create', [App\Http\Controllers\AdminController::class,'createInvoiceView'])->name('createInvoiceView');
        Route::post('/create', [App\Http\Controllers\AdminController::class,'createInvoice']);
        Route::get('/all', [App\Http\Controllers\AdminController::class,'allInvoiceView'])->name('allInvoice');
        Route::get('/user', [App\Http\Controllers\AdminController::class,'userInvoice'])->name('userInvoice');
        Route::post('/user', [App\Http\Controllers\AdminController::class,'userInvoiceSubmit']);
    });
});

Route::get('/dashboard', [App\Http\Controllers\UserController::class,'dashboard'])->name('dashboard');
Route::get('/search/item', [App\Http\Controllers\UserController::class,'searchItemView'])->name('searchItem');
Route::get('/all/item', [App\Http\Controllers\UserController::class,'allItem'])->name('allItem');
Route::get('/all/unknown', [App\Http\Controllers\UserController::class,'allUnknown'])->name('allUnknown');
Route::post('/all/unknown', [App\Http\Controllers\UserController::class,'submitUnknown']);
Route::get('/all/unknown/request', [App\Http\Controllers\UserController::class,'allUnknownRequest'])->name('allUnknownRequest');
Route::get('/all/unknown/request/datatable', [App\Http\Controllers\UserController::class,'unknownrequestDatatable'])->name('unknownrequestDatatable');
Route::get('/profile', [App\Http\Controllers\UserController::class,'profile'])->name('profile');
Route::post('/profile', [App\Http\Controllers\UserController::class,'update']);
Route::post('/password', [App\Http\Controllers\UserController::class,'updatepassword'])->name('password');
Route::get('/search/item/details', [App\Http\Controllers\UserController::class,'searchItemDetails'])->name('searchItemDetails');
Route::get('/manifest', [App\Http\Controllers\UserController::class,'userManifest'])->name('userManifest');
Route::get('/bag', [App\Http\Controllers\UserController::class,'userBag'])->name('userBag');
Route::get('/report', [App\Http\Controllers\UserController::class,'userReport'])->name('userReport');
Route::get('/manifest/generate', [App\Http\Controllers\UserController::class,'userManifestGenerateView'])->name('userManifestGenerateView');
Route::get('/report/generate/download', [App\Http\Controllers\UserController::class,'userReportGenerateDownload'])->name('userReportGenerateDownload');
Route::get('/report/datatable', [App\Http\Controllers\UserController::class,'userReportDatatable'])->name('userReportDatatable');
Route::get('/report/generate/datatable', [App\Http\Controllers\UserController::class,'userReportGenerateDatatable'])->name('userReportGenerateDatatable');
Route::get('/bag/view/{id}', [App\Http\Controllers\UserController::class,'userBagView'])->name('userBagView');
Route::get('/manifest/download', [App\Http\Controllers\UserController::class,'userManifestDownload'])->name('userManifestDownload');
Route::get('/report/download', [App\Http\Controllers\UserController::class,'userReportDownload'])->name('userReportDownload');

Auth::routes();