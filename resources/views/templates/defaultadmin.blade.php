<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Speedzmaster Warehouse | Home</title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<meta name="author" content="" />
    <link href="{{ url('/') }}/panel/css/app.min.css?version=4" rel="stylesheet" />
    <link href="{{ url('/') }}/panel/css/style.css?version=4" rel="stylesheet" />
</head>
    <body>
        <div id="app" class="app">
		<div id="header" class="app-header">
			<div class="mobile-toggler">
				<button type="button" class="menu-toggler" data-toggle="sidebar-mobile">
					<span class="bar"></span>
					<span class="bar"></span>
				</button>
			</div>
			<div class="brand">
				<div class="desktop-toggler">
					<button type="button" class="menu-toggler" data-toggle="sidebar-minify">
						<span class="bar"></span>
						<span class="bar"></span>
					</button>
				</div>
				
				<a href="#" class="brand-logo">
					<img src="{{ url('/') }}/panel/img/logo.png" alt="" height="60" />
				</a>
			</div>
			@include('templates.defaultmenu')
		</div>
        	@include('templates.sidenav')
            <div id="content" class="app-content">
                @yield('content')
            </div>
            <a href="#" data-click="scroll-top" class="btn-scroll-top fade"><i class="fa fa-arrow-up"></i></a>
        </div>
        @yield('scripts')
        <script src="{{ url('/') }}/panel/js/app.min.js"></script>
    </body>
</html>