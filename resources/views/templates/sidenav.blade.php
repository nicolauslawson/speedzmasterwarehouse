<div id="sidebar" class="app-sidebar">
    <!-- BEGIN scrollbar -->
    <div class="app-sidebar-content" data-scrollbar="true" data-height="100%">
        <!-- BEGIN menu -->
        <div class="menu">
            <div class="menu-item active">
                <a href="{{route('dashboard')}}" class="menu-link">
                    <span class="menu-icon"><i class="fas fa-tachometer-alt"></i></span>
                    <span class="menu-text">Dashboard</span>
                </a>
            </div>
            <div class="menu-divider"></div>
            <div class="menu-item has-sub">
                <a href="#" class="menu-link">
                    <span class="menu-icon"><i class="fas fa-boxes"></i></span>
                    <span class="menu-text">Item Management</span> 
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="{{ route('addItem') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">Add Item</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="{{ route('viewQuery') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">Query</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="{{ route('searchItemAdminView') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">Search Item</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="{{ route('adminAllUnknown') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">Unknown Items</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="{{ route('viewAllItem') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">View All</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-item has-sub">
                <a href="#" class="menu-link">
                    <span class="menu-icon"><i class="fas fa-layer-group"></i></span>
                    <span class="menu-text">Manifest Management</span> 
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="{{ route('createManifestView') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">Generate Manifest</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-item has-sub">
                <a href="#" class="menu-link">
                    <span class="menu-icon"><i class="fa fa-shopping-bag"></i></span>
                    <span class="menu-text">Bag Management</span>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="{{ route('createBag') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">Create Bag Label</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="{{ route('packBagView') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">Pack Bag</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="{{ route('checkBagView') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">Check Bag</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="{{ route('allBagView') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">All Bags</span>
                        </a>
                    </div>
                </div>
            </div>
            @if (Auth::user()->type == 1)
                <div class="menu-item has-sub">
                    <a href="#" class="menu-link">
                        <span class="menu-icon"><i class="fas fa-dollar-sign"></i></span>
                        <span class="menu-text">Billing</span> 
                    </a>
                    <div class="menu-submenu">
                        <div class="menu-item">
                            <a href="{{ route('createInvoiceView') }}" class="menu-link">
                                <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">Courier Invoice</span>
                            </a>
                            <a href="{{ route('userInvoice') }}" class="menu-link">
                                <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">User Invoice</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="menu-item has-sub">
                    <a href="#" class="menu-link">
                        <span class="menu-icon"><i class="far fa-file-excel"></i></span>
                        <span class="menu-text">Audit Trail</span> 
                    </a>
                    <div class="menu-submenu">
                        <div class="menu-item">
                            <a href="{{ route('packageaudit') }}" class="menu-link">
                                <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">Package Audit</span>
                            </a>
                        </div>
                    </div>
                </div>
            @endif
            <div class="menu-divider"></div>
            <div class="menu-header">User Management</div>
            <div class="menu-item">
                <a href="{{ route('viewAllUser') }}" class="menu-link">
                    <span class="menu-icon"><i class="fas fa-users"></i></span>
                    <span class="menu-text">Inspect User</span>
                </a>
            </div>
            <div class="menu-item">
                <a href="{{ route('viewAllCourier') }}" class="menu-link">
                    <span class="menu-icon"><i class="far fa-building"></i></span>
                    <span class="menu-text">Inspect Courier</span>
                </a>
            </div>

            {{-- <div class="menu-divider"></div>
            <div class="menu-header">Courier Service</div>
            <div class="menu-item">
                <a href="{{ route('searchuser') }}" class="menu-link">
                    <span class="menu-icon"><i class="fa fa-search"></i></span>
                    <span class="menu-text">Search User</span>
                </a>
            </div> --}}
        </div>
        <!-- END menu -->
    </div>
    <!-- END scrollbar -->
    
    <!-- BEGIN mobile-sidebar-backdrop -->
    <button class="app-sidebar-mobile-backdrop" data-dismiss="sidebar-mobile"></button>
    <!-- END mobile-sidebar-backdrop -->
</div>