<!-- BEGIN menu -->
<div class="menu">
	<form class="menu-search">
	</form>
	<div class="menu-item dropdown">
		<a href="#" data-toggle="dropdown" data-display="static" class="menu-link">
			<div class="menu-text" style="color: white">{{Auth::user()->courier}}</div>
		</a>
		<div class="dropdown-menu dropdown-menu-right mr-lg-3">
			<a class="dropdown-item d-flex align-items-center" href="{{route('profile')}}">Profile <i class="fa fa-user-circle fa-fw ml-auto text-gray-400 f-s-16"></i></a>
			<div class="dropdown-divider"></div>
			<a class="dropdown-item d-flex align-items-center" href="{{ route('logout') }}"
							onclick="event.preventDefault();
											document.getElementById('logout-form').submit();">
							Log Out <i class="fa fa-toggle-off fa-fw ml-auto text-gray-400 f-s-16"></i></a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
			</form>
		</div>
	</div>
</div>
<!-- END menu -->