<div id="sidebar" class="app-sidebar">
    <!-- BEGIN scrollbar -->
    <div class="app-sidebar-content" data-scrollbar="true" data-height="100%">
        <!-- BEGIN menu -->
        <div class="menu">
            <div class="menu-header">Dashboard</div>
            <div class="menu-item active">
                <a href="{{route('dashboard')}}" class="menu-link">
                    <span class="menu-icon"><i class="fas fa-tachometer-alt"></i></span>
                    <span class="menu-text">Dashboard</span>
                </a>
            </div>
            <div class="menu-divider"></div>
            <div class="menu-item has-sub">
                <a href="#" class="menu-link">
                    <span class="menu-icon"><i class="fa fa-box"></i></span>
                    <span class="menu-text">Item Management</span> 
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="{{ route('allItem') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">All Packages</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="{{ route('userReport') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">Daily Packages</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="{{ route('allUnknown') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">Unknown Packages</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-item has-sub">
                <a href="#" class="menu-link">
                    <span class="menu-icon"><i class="fas fa-layer-group"></i></span>
                    <span class="menu-text">Manifest Management</span> 
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="{{ route('userManifestGenerateView') }}" class="menu-link">
                            <i class="fas fa-long-arrow-alt-right">&nbsp</i><span class="menu-text">Generate Manifest</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- END menu -->
    </div>
    <!-- END scrollbar -->
    
    <!-- BEGIN mobile-sidebar-backdrop -->
    <button class="app-sidebar-mobile-backdrop" data-dismiss="sidebar-mobile"></button>
    <!-- END mobile-sidebar-backdrop -->
</div>