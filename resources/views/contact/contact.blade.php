<h3>Contact Form Message</h3>
<div>
  <p>Name: {{$name}}</p>
  <p>Email: {{$email}}</p>
  <p>Phone: {{$phone}}</p>
  <p>Subject: {{$subject}}</p>
  <p>Message: {{$comment}}</p>
</div>
