@extends('templates.default')
@section('headscript')
<link rel="stylesheet" href="{{ url('/') }}/css/login.css" type="text/css" media="all" />
@endsection
@section('content')
<div class="" id="home">
    <header>
        <div class="container-fluid">
            <div class="header d-md-flex justify-content-between align-items-center py-3 px-xl-5 px-lg-3 px-2">
                <div id="logo">
                    <a href="/"><img src="{{ url('/') }}/img/logo.png" height="90" alt="Speedzmaster Warehouse"></a>
                </div>
            </div>
        </div>
    </header>
    <!-- //header -->
</div>
<div class="container h-100">
    <div class="d-flex justify-content-center h-100">
        <div class="user_card">
            <div class="d-flex justify-content-center">
                <div class="brand_logo_container">
                    <img src="{{ url('/') }}/img/logo.png" class="brand_logo" height="100" alt="Logo">
                </div>
            </div>
            <div class="d-flex justify-content-center form_container_reset">
                <form>
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" name="email" class="form-control input_user" placeholder="Email">
                    </div>
                </form>
            </div>
            <div class="d-flex justify-content-center mt-3 login_container">
                <button type="button" name="button" class="btn login_btn">Login</button>
            </div>
        </div>
    </div>
</div>
@include('templates.defaultfooter')
@endsection
