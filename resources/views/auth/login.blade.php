@extends('templates.defaultauth')
@section('content')
  <nav class="navbar">
      <div class="container-fluid">
      <div class="navbar-header">
          {{-- <a class="navbar-brand" href="/">Speedzmaster Warehouse</a> --}}
      </div>
      <div>
          <a class="navbar-brand text-white" href="{{ url('/') }}">Home</a>
      </div>
      </div>
  </nav>
  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <a href="/" class="logo"><img src="{{ url('/') }}/img/logo.png" class="img-fluid" style="height: 120px;"></a>
                    <h1 class="h4 text-gray-900 mb-4 mt-2">Welcome Back!</h1>
                  </div>
                  <form class="user" action="{{route('login')}}" method="post">
                    <div class="form-group">
                      <input type="email" class="form-control form-control-user" name='email' placeholder="Enter Email Address...">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" name='password' placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block text-white">
                      Login
                    </button>
                    @csrf
                  </form>
                  <hr>
                  {{-- <div class="text-center">
                    <a class="small" href="{{route('password.request')}}">Forgot Password?</a>
                  </div>
                  <div class="text-center">
                  <a class="small" href="{{route('register')}}">Create an Account!</a>
                  </div> --}}
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
@endsection