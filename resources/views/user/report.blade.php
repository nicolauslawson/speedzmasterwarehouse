@extends('templates.defaultuser')
@section('content')
  <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">Daily Report</h1>
        <p class="mb-4">Download the report for a specific day</p>
        Item Total ({{$date}}): <span class="text-danger">{{$items->total()}}</span> <br>
        Total Weight ({{$date}}): <span class="text-danger">{{ number_format($weight, 2, '.', ',') }} lbs |  {{ number_format($weight*0.453592, 2, '.', ',') }} kg</span><br>
        <hr>
        <div class="card shadow mb-4 mt-5">
            <div class="card-header py-3">
                @if (!app('request')->input('date'))
                    <h6 class="m-0 font-weight-bold text-primary">Daily Report Result for {{$date}}</h6>
                @else
                    <h6 class="m-0 font-weight-bold text-primary">Daily Report Result for {{app('request')->input('date')}}</h6>
                @endif
            </div>
            <div class="card-body">
                <form action="">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="">Report Date</label>
                            <input id="report_date" name="date" type="date" value="{{app('request')->input('date')}}" class="form-control">
                        </div>
                        <div class="col-sm-3 form-group">
                            <label for="">&nbsp</label>
                            <button type="button" onclick="clearDate()" class="form-control btn-warning text-white">Clear</button>
                        </div>
                        <div class="col-sm-3 form-group">
                            <label for="">&nbsp</label>
                            <button id="submitDateButton" type="submit" class="form-control btn-info text-white">Generate</button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="view-search">
                    <thead>
                    <tr>
                        <th>Tracking Number</th>
                        <th>Mailbox</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Description</th>
                        <th>Weight</th>
                        <th>AWB</th>
                        <th>Shipper</th>
                        <th>Package Type</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                        <tr>
                            <td>{{$item->tracking_number}}</td>
                            <td>{{$item->prefix}}</td>
                            <td>{{$item->first_name}}</td>
                            <td>{{$item->last_name}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->weight}}</td>
                            <td>{{$item->house_number}}</td>
                            <td>{{$item->shipper}}</td>
                            <td>{{$item->package_type}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        <form action="{{route('userReportDownload')}}" method="get">
              <input type="hidden" name="date" value="{{app('request')->input('date')}}">
              <button type="submit" class="btn btn-primary">Download</button>
        </form>
  </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ url('/') }}/js/jquery.min.js"></script>
    
    <script>
        function clearDate() {
            document.getElementById("report_date").value = new Date();
            document.getElementById("submitDateButton").click();
        }
    </script>
@endsection