@extends('templates.defaultuser')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">All Unknown</h1>
        <p class="mb-4">Table showing list of all unknown items</p>

        @if(!empty($errors->first()))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first() }}
            </div>
        @endif
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">All Unknown Items - Click the request button to start</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="view-all-unknown">
                    <thead>
                    <tr>
                        <th>Tracking Number</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Weight</th>
                        <th>Shipper</th>
                        <th></th>
                    </tr>
                    </thead>
                    @foreach ($items as $key => $element)
                        <tr>
                            <td>....{{ substr($element->tracking_number, -8)}}</td>
                            <td>{{$element->first_name}}</td>
                            <td>{{$element->last_name}}</td>
                            <td>{{$element->weight}}</td>
                            <td>{{$element->shipper}}</td>
                            <td>
                                <a role="button" class="btn btn-sm btn-warning text-white" id="modalVal-{{$key}}" data-first_name='{{$element->first_name}}' data-last_name='{{$element->last_name}}' data-weight='+data.weight+' data-id='{{$element->id}}' data-toggle="modal" data-target="#unknownModal" onClick="modalClick({{$element->id}})">Request</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="unknownModal" tabindex="-1" role="dialog" aria-labelledby="unknownModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="post" action="{{ route('allUnknown') }}">
                    <div class="modal-header">
                        <h5 class="modal-title" id="unknownModalTitle">Request Item</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="Modalname"></div>
                        <hr style="border: 1px solid black;">
                        <label for="">Tracking Number</label>
                        <input type="text" name="tracking_number" class="form-control">
                        <small class="text-danger">* Please enter the full tracking number</small>
                        <input type="hidden" name="item_id" id="item_id" value=""/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ url('/') }}/vendor/jquery/jquery.min.js"></script>

    <script>
        function modalClick(id) {
            var first_name = $('#modalVal-'+id).data('first_name');
            var last_name = $('#modalVal-'+id).data('last_name');
              $(".modal-body #item_id").val(id);
            document.getElementById("Modalname").innerHTML = 'Name: '+first_name+' '+last_name;
        }
    </script>
@endsection
