@extends('templates.defaultuser')
@section('content')
  <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">Generate Manifest</h1>
        <hr>
        @if(session()->has('message'))
            <div class="alert alert-danger">
                {{ session()->get('message') }}
            </div>
        @endif
            <div class="card shadow mb-4 mt-5">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Generated Manifest From <span class="text-warning">{{app('request')->input('start_date')}}</span> to <span class="text-warning">{{app('request')->input('end_date')}}</span> <a href="{{route('userManifestDownload', ['start' => app('request')->input('start_date'), 'end' => app('request')->input('end_date')])}}" class="btn btn-danger float-right">Download</a></h6>
                </div>
                <div class="card-body">
                    <form action="">
                    <div class="row">
                            <div class="col-sm-5 form-group">
                                <label for="">Report Date</label>
                                <input id="start_date" name="start_date" type="date" value="{{app('request')->input('start_date')}}" class="form-control">
                            </div>
                            <div class="col-sm-5 form-group">
                                <label for="">Report Date</label>
                                <input id="end_date" name="end_date" type="date" value="{{app('request')->input('end_date')}}" class="form-control">
                            </div>
                            <div class="col-sm-2 form-group">
                                <label for="">&nbsp</label>
                                <button type="submit" class="form-control btn-info text-white">Generate</button>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                    <table class="table table-bordered" width="100%" cellspacing="0" id="view-search">
                        <thead>
                        <tr>
                            <th>Tracking Number</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Description</th>
                            <th>Weight</th>
                            <th>AWB</th>
                            <th>Shipper</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $item)
                            <tr>
                                <td>{{$item->tracking_number}}</td>
                                <td>{{$item->first_name}}</td>
                                <td>{{$item->last_name}}</td>
                                <td>{{$item->description}}</td>
                                <td>{{$item->weight}}</td>
                                <td>{{$item->house_number}}</td>
                                <td>{{$item->shipper}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            {{ $items->appends(request()->input())->links("pagination::bootstrap-4") }}
                        </ul>
                    </nav>
                    </div>
                </div>
            </div>
  </div>
@endsection