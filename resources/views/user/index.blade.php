@extends('templates.defaultuser')
@section('content')
        <div class="container-fluid">

          <!-- Content Row -->
          <div class="row">
              <div class="col-sm-8 col-md-8 col-lg-12 am-dl">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Recent Packages</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-bordered" width="100%" cellspacing="0" id="recent-item">
                            <thead>
                            <tr>
                                <th>Tracking Number</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Description</th>
                                <th>Weight</th>
                                <th>AWB</th>
                                <th>Shipper</th>
                                <th>Package Type</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($items as $item)
                                <tr>
                                    <td>{{$item->tracking_number}}</td>
                                    <td>{{$item->first_name}}</td>
                                    <td>{{$item->last_name}}</td>
                                    <td>{{$item->description}}</td>
                                    <td>{{$item->weight}}</td>
                                    <td>{{$item->house_number}}</td>
                                    <td>{{$item->shipper}}</td>
                                    <td>{{$item->package_type}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                {{ $items->appends(request()->input())->links("pagination::bootstrap-4") }}
                            </ul>
                        </nav>
                        </div>
                    </div>
                </div>
              </div>
              {{-- <div class="col-4">
                <div class="card">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Useful Information</h6>
                    </div>
                    <div class="card-body">
                        <p class="card-text">Today (Item): <b>{{$recent}}</b></p>
                        <p class="card-text">Item total: <b>{{$invent}}</b></p>
                        <p class="card-text">Manifest Total: <b>{{$manifest}}</b></p>
                    </div>
                </div>
              </div> --}}
          </div>
        </div>
@endsection
