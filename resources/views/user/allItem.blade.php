@extends('templates.defaultuser')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">All Packages</h1>
        <p class="mb-4">Table showing list of all packages</p>
        Item Total: <span class="text-danger">{{$count}}</span> <br>
        Total Weight: <span class="text-danger">{{ number_format($weight, 2, '.', ',') }} lbs |  {{ number_format($weight*0.453592, 2, '.', ',') }} kg</span><br>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">All Packages</h6>
            </div>
            <div class="card-body">
                <form action="">
                    <div class="row">
                        <div class="col-sm-2 form-group">
                            <label for="">First Name</label>
                            <input id="first_name" type="text" value="{{app('request')->input('fn')}}" name="fn" class="form-control">
                        </div>
                        <div class="col-sm-2 form-group">
                            <label for="">Last Name</label>
                            <input id="last_name" type="text" value="{{app('request')->input('ln')}}" name="ln" class="form-control">
                        </div>
                        <div class="col-sm-2 form-group">
                            <label for="">Tracking Number</label>
                            <input id="tracking" type="text" value="{{app('request')->input('tracking')}}" name="tracking" class="form-control">
                        </div>
                        <div class="col-sm-2 form-group">
                            <label for="">Item Number</label>
                            <input id="awb" type="text" value="{{app('request')->input('awb')}}" name="awb" class="form-control">
                        </div>
                        <div class="col-sm-2 form-group">
                            <label for="">&nbsp</label>
                            <button type="button" onclick="clearData()" class="form-control btn-warning text-white">Clear</button>
                        </div>
                        <div class="col-sm-2 form-group">
                            <label for="">&nbsp</label>
                            <button id="submitButton" type="submit" class="form-control btn-info text-white">Search</button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="view-all-item">
                    <thead>
                    <tr>
                        <th>Tracking Number</th>
                        <th>Mailbox</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Description</th>
                        <th>Weight</th>
                        <th>AWB</th>
                        <th>Bag</th>
                        <th>Shipper</th>
                        <th>Package Type</th>
                        <th>Created</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                        <tr>
                            <td>{{$item->tracking_number}}</td>
                            <td>{{$item->prefix}}</td>
                            <td>{{$item->first_name}}</td>
                            <td>{{$item->last_name}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->weight}}</td>
                            <td>{{$item->house_number}}</td>
                            <td>{{$item->bag_number}}</td>
                            <td>{{$item->shipper}}</td>
                            <td>{{$item->package_type}}</td>
                            <td>{{$item->created_at}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        {{ $items->appends(request()->input())->links("pagination::bootstrap-4") }}
                    </ul>
                </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ url('/') }}/js/jquery.min.js"></script>
    
    <script>
        function clearData() {
            document.getElementById("first_name").value = "";
            document.getElementById("last_name").value = "";
            document.getElementById("tracking").value = "";
            document.getElementById("awb").value = "";
            document.getElementById("submitButton").click();
        }
    </script>
@endsection