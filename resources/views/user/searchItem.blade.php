@extends('templates.defaultuser')
@section('content')
  <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">Search Item</h1>
        <p class="mb-4">Search for customer item</p>
        <hr>
        <form method="get" action="{{ route('searchItem') }}">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>First Name</label>
                    <input type="text" name="fn" class="{{ $errors->has('fn') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                </div>
                <div class="form-group col-md-6">
                    <label>Last Name</label>
                    <input type="text" name="ln" class="{{ $errors->has('ln') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Tracking Number</label>
                    <input type="text" name="tracking" class="{{ $errors->has('tracking') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
        @if (app('request')->input('tracking') || app('request')->input('fn') || app('request')->input('ln'))
        <div class="card shadow mb-4 mt-5">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Search Result</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="view-search">
                    <thead>
                        <tr>
                            <th>Tracking Number</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Description</th>
                            <th>Weight</th>
                            <th>AWB</th>
                            <th>Shipper</th>
                            <th>Bag</th>
                            <th>Package Type</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                </table>
                </div>
            </div>
        </div>
        @endif
  </div>
@endsection
@section('scripts')
    <script src="{{ url('/') }}/vendor/jquery/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
                $('#view-search').DataTable({
                    processing: true,
                    responsive: false,
                    ajax: '{{ url('/') }}/search/item/details?fn={{app('request')->input('fn')}}&ln={{app('request')->input('ln')}}&tracking={{app('request')->input('tracking')}}',
                    "lengthMenu": [[50, 80, 100], [50, 80, 100]],
                    columns: [
                        { data: 'tracking_number', name: 'tracking_number' },
                        { data: 'first_name', name: 'first_name' },
                        { data: 'last_name', name: 'last_name' },
                        { data: 'description', name: 'description' },
                        { data: 'weight', name: 'weight' },
                        { data: 'house_number', name: 'house_number' },
                        { data: 'shipper', name: 'shipper' },
                        { data: 'bag_number', name: 'bag_number' },
                        { data: 'package_type', name: 'package_type' },
                        { data: 'created_at', name: 'created_at' },
                    ]
                });
            });
    </script>
@endsection
