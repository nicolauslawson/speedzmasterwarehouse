<!DOCTYPE html>
<html>
<head>
	<title>Speedzmaster Warehouse</title>
	<link rel="stylesheet" href="{{ url('/') }}/css/404style.css">
	<link href='//fonts.googleapis.com/css?family=Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	<!-- For-Mobile-Apps-and-Meta-Tags -->
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- //For-Mobile-Apps-and-Meta-Tags -->
<script src="{{ url('/') }}/js/jquery-1.11.1.min.js"></script>
</head>
<body>
	<div class="main">
		<div class="agile">
			<div class="agileits_main_grid">
				<div class="agileits_main_grid_left">
					<h1>Speedzmaster Warehouse</h1>
				</div>
				<div class="clear"> </div>
			</div>
			<div class="w3l">
				<div class="text">
					<h1>PAGE NOT FOUND</h1>

					<p>You have been tricked into click on a link that can not be found. Please check the url or go to <a href="{{ url('/') }}">main page</a> and see if you can locate what you are looking for</p>
				</div>
				<div class="image">
					<img src="{{ url('/') }}/img/smile.png">
				</div>
				<div class="clear"></div>
			</div>
				<div class="footer">
					<p>&copy; 2021 Speedzmaster Warehouse. All Rights Reserved | Site by <a href="http://zhultek.com">Zhultek</a></p>
				</div>
				<div class="wthree">
					<div class="back">
						<a href="{{ url('/') }}">Back to home</a>
					</div>
					<div class="clear"></div>
				</div>
		</div>
	</div>
</body>
</html>
