@extends('layouts.default')
@section('content')
<div class="container-fluid main">
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a href="/" class="logo"><img src="{{ url('/') }}/img/logo.png" class="img-fluid" style="height: 100px;"></a>
      </div>
      <div>
        @auth
            <a class="navbar-brand" href="{{ url('/dashboard') }}">Home</a>
        @else
            <a class="navbar-brand" href="{{ route('login') }}">Login</a>
        @if (Route::has('register'))
            {{-- <a class="navbar-brand" href="{{ route('register') }}">Register</a> --}}
        @endif
        @endauth
      </div>
    </div>
  </nav>
  <div id="myCarousel" class="carousel carousel-fade slide" data-ride="carousel" data-interval="3000">
    <div class="carousel-inner" role="listbox">
      <div class="item active background a"></div>
    </div>
  </div>

  <div class="covertext">
    <div class="col-lg-10" style="float:none; margin:0 auto;">
      <h1 class="title">Speedz Warehouse</h1>
      <h3 class="subtitle">We are a professional team of cargo handlers</h3>
    </div>
    <div class="col-xs-12 explore mt-4">
      <a href="{{ route('login') }}"><button type="button" class="btn btn-lg explorebtn">Login</button></a>
    </div>
  </div>
</div>
@endsection