

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <style>
    @page {
      margin: 10px;
    }
    #bgimg {
      position: fixed;
      left: -.5in;
      top: -.5in;
      width: 8.5in;
      height: 11in;
      z-index: -999
    }
  </style>
</head>
<body>
    <div>
        <p style="font-size: 22px">{{$data[0]->courier}} <span style="font-weight: bold; float: right;">{{$data[0]->weight}} lb</span></p>
        <hr style="border: 1px solid black;">
        <p style="font-size: 30px">{{$data[0]->first_name}} {{$data[0]->last_name}}</p>
        @if ($data[0]->type == 'Single')
          @php
              echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($data[0]->barcode, 'C39',2,190) . '" alt="barcode"   />';
          @endphp
        @else
          @php
              echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($data[0]->barcode, 'C39',1,190) . '" alt="barcode"   />';
          @endphp
        @endif
        <p style="font-size: 23px">{{$data[0]->barcode}}<span style="font-weight: bold; float: right;">Kingston</span></p>
    </div>
</body>
</html>