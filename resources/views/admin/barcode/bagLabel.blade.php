

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <style>
    @page {
      margin: 10px;
    }
    #bgimg {
      position: fixed;
      left: -.5in;
      top: -.5in;
      width: 8.5in;
      height: 11in;
      z-index: -999
    }
  </style>
</head>
<body>
    <div>
        <p style="font-size: 16px">{{$bag->courier}}</p>
        <hr style="border: 1px solid black;">
        <p style="font-size: 20px">{{$bag->bag_number}}</p>
        @php
            echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($bag->bag_number, 'C39',1.70,85) . '" alt="barcode"   />';
        @endphp
        <p style="font-size: 20px"><span style="font-weight: bold; float: right;">Kingston</span></p>
    </div>
</body>
</html>