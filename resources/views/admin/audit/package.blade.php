@extends('templates.defaultadmin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">User Item Count</h1>
        <p class="mb-4">Table showing list of items entered by staff for TODAY</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">User Item Count</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="all-user">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Item Count</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $element)
                            <tr>
                                <td>{{$element->created_by}}</td>
                                <td>{{$element->items}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        {{ $data->appends(request()->input())->links("pagination::bootstrap-4") }}
                    </ul>
                </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
