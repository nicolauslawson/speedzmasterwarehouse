@extends('templates.defaultadmin')
@section('content')
  <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add Courier</h1>
        </div>
        <hr>
        <form method="post" action="{{ route('addCourier') }}">
            <div class="form-row">
                <div class="form-group col-6">
                    <label>Company Name</label>
                    <input type="text" name="company_name" class="{{ $errors->has('company_name') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('company_name'))
                        <span class="text-danger">Company Name is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>Abbr</label>
                    <input type="text" name="abbr" class="{{ $errors->has('abbr') ? ' form-control is-invalid' : 'form-control' }}" maxlength="5">
                    @if($errors->has('abbr'))
                        <span class="text-danger">Abbr is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-6">
                    <label>Primary Address</label>
                    <input type="text" name="primary_address" class="{{ $errors->has('primary_address') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('primary_address'))
                        <span class="text-danger">Address is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>Secondary Address</label>
                    <input type="text" name="secondary_address" class="{{ $errors->has('secondary_address') ? ' form-control is-invalid' : 'form-control' }}">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-sm">
                    <label>Country</label>
                    <input type="text" name="country" class="{{ $errors->has('country') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('country'))
                        <span class="text-danger">Country is required</span>
                    @endif
                </div>
                <div class="form-group col-sm">
                    <label>City</label>
                    <input type="text" name="city" class="{{ $errors->has('city') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('city'))
                        <span class="text-danger">City is required</span>
                    @endif
                </div>
                <div class="form-group col-sm">
                    <label>Parish</label>
                    <input type="text" name="parish" class="{{ $errors->has('parish') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('parish'))
                        <span class="text-danger">Parish is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-6">
                    <label>Mobile #1</label>
                    <input type="text" name="mobile_1" class="{{ $errors->has('mobile_1') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('mobile_1'))
                        <span class="text-danger">Mobile #1 is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>Mobile #2</label>
                    <input type="text" name="mobile_2" class="{{ $errors->has('mobile_2') ? ' form-control is-invalid' : 'form-control' }}">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            {{ csrf_field() }}
        </form>
  </div>
@endsection
