@extends('templates.defaultadmin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">All Couriers <span class="float-right"><a role="button" href="{{route('addCourier')}}" class="btn btn-primary">Add Courier</a></span></h1>
        <p class="mb-4">Table showing list of all couriers</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">All Couriers</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="all-user">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Abbr</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>Mobile</th>
                            <th>View</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($courier as $element)
                            <tr>
                                <td>{{$element->company_name}}</td>
                                <td>{{$element->abbr}}</td>
                                <td>{{$element->primary_address}}</td>
                                <td>{{$element->city}}</td>
                                <td>{{$element->mobile_cell}}</td>
                                <td><a role="button" class="btn btn-info text-white" href="{{route('viewSingleCourier', ['id'=>$element->id])}}">View</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        {{ $courier->appends(request()->input())->links("pagination::bootstrap-4") }}
                    </ul>
                </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
