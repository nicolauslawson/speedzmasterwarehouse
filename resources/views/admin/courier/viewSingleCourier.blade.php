@extends('templates.defaultadmin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">Courier Details</h1>
        <p class="mb-4">Section showing details for {{$courier->company_name}}</p>
    </div>
    <hr>
    @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
    @endif
    @if(session()->has('block'))
            <div class="alert alert-danger">
                {{ session()->get('block') }}
            </div>
    @endif
    @if(session()->has('unblock'))
            <div class="alert alert-success">
                {{ session()->get('unblock') }}
            </div>
    @endif
    <div class="row">
        <div class="col-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Details for {{$courier->company_name }}</h6>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('updateCourier') }}">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                            <label>Company Name</label>
                            <input type="text" name="first_name" value="{{$courier->company_name}}" class="form-control" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" name="address" value="{{$courier->primary_address}}" class="form-control">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm">
                            <label>City</label>
                            <input type="text" name="city" value="{{$courier->city}}" class="form-control">
                            </div>
                            <div class="form-group col-sm">
                            <label>Parish</label>
                            <input type="text" name="parish" value="{{$courier->parish}}" class="form-control">
                            </div>
                            <div class="form-group col-sm">
                            <label>Country</label>
                            <input type="text" name="country" value="{{$courier->country}}" class="form-control">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label>Mobile #1</label>
                            <input type="text" name="mobile_cell" value="{{$courier->mobile_cell}}" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                            <label>Mobile #2</label>
                            <input type="text" name="mobile_other" value="{{$courier->mobile_other}}" class="form-control">
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$courier->id}}">
                        <button type="submit" class="btn btn-primary">Update</button>
                        @if ($status->status == 'Inactive')
                            <a role="button" href="{{ route('statusUser', ['id'=>$courier->id]) }}" class="btn btn-warning">Make Active</a>
                        @else
                            <a role="button" href="{{ route('statusUser', ['id'=>$courier->id]) }}" class="btn btn-danger">Make Inactive</a>
                        @endif
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Billing Details</h6>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('billingupdate') }}">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                            <label>Bags & Seals</label>
                                <input type="text" name="bags_cost" value="{{$courier->bags_cost}}" class="form-control">
                            </div>
                            
                            <div class="form-group col-md-4">
                            <label>AWB Cost</label>
                                <input type="text" name="awb_cost" value="{{$courier->awb_cost}}" class="form-control">
                            </div>

                            <div class="form-group col-md-4">
                            <label>Delivery</label>
                                <input type="text" name="delivery_cost" value="{{$courier->delivery_cost}}" class="form-control">
                            </div>
                        <div class="form-group col-md-6">
                            <label>0-10lbs</label>
                                <input type="text" name="tier1" value="{{$courier->tier1}}" class="form-control">
                            </div>
                            
                            <div class="form-group col-md-6">
                            <label>11-20lbs</label>
                                <input type="text" name="tier2" value="{{$courier->tier2}}" class="form-control">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label>21-50lbs</label>
                                <input type="text" name="tier3" value="{{$courier->tier3}}" class="form-control">
                            </div>
                            
                            <div class="form-group col-md-6">
                            <label>51-100lbs</label>
                                <input type="text" name="tier4" value="{{$courier->tier4}}" class="form-control">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label>101-150lbs</label>
                                <input type="text" name="tier5" value="{{$courier->tier5}}" class="form-control">
                            </div>
                            
                            <div class="form-group col-md-6">
                            <label>151+</label>
                                <input type="text" name="tier6" value="{{$courier->tier6}}" class="form-control">
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$courier->id}}">
                        <button type="submit" class="btn btn-primary">Update Billing</button>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">{{$courier->company_name }} API Details <span><a href="/doc/api_structure.pdf" role="button" class="btn btn-sm btn-secondary pull-right" download>API Document</a></span></h6>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('generateApiKey') }}">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                            <label>API Key</label>
                            @if ($courier->api()->exists())
                                <input type="text" name="key" value="{{$courier->api->app_key}}" class="form-control" disabled>
                            @else
                                <input type="text" name="key" value="" class="form-control" disabled>
                            @endif
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$courier->id}}">
                        <button type="submit" class="btn btn-primary">Generate API Key</button>
                        @if ($courier->api()->exists() && $courier->api->status == 1)
                            <a role="button" href="{{ route('apiStatusUpdate', ['id'=>$courier->id, 'status'=>0]) }}" class="btn btn-danger">Block</a>
                        @else
                            <a role="button" href="{{ route('apiStatusUpdate', ['id'=>$courier->id, 'status'=>1]) }}" class="btn btn-warning">Unblock</a>
                        @endif
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Additional Information</h6>
                </div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                        <label>Created Date: <b>{{ \Carbon\Carbon::parse($courier->created_at)->format('F d, Y')}}</b></label><br>
                        <label>Days a Member: <b>{{ \Carbon\Carbon::parse($courier->created_at)->diffForHumans()}}</b></label><br>
                        @if ($status->status == "Active")
                            <label>Courier Status: <b class="text-success">{{$status->status}}</b></label>
                        @else
                            <label>Courier Status: <b class="text-danger">{{$status->status}}</b></label>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="blockUserModal" tabindex="-1" role="dialog" aria-labelledby="blockUserModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="blockUserModalLabel">Block User!!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you want to <b>BLOCK</b> this user?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="deleteUserModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteUserModalLabel">Delete User!!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you want to <b>DELETE</b> this user?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
@endsection