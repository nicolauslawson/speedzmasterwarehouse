@extends('templates.defaultadmin')
@section('content')
  <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">Generate Manifest</h1>
        <p class="mb-4">Create manifest for different couriers</p>
        @if(session()->has('message'))
                <div class="alert alert-success" role="alert">
                    {{ session()->get('message') }}
                </div>
        @endif
        <form>
            <div class="form-row">
                <div class="form-group col-6">
                    <label>Start Date</label>
                    <input type="date" name="start_date" value="{{app('request')->input('start_date')}}" class="{{ $errors->has('start_date') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('start_date'))
                        <span class="text-danger">Start Date is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>End Date</label>
                    <input type="date" name="end_date" value="{{app('request')->input('end_date')}}" class="{{ $errors->has('end_date') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('end_date'))
                        <span class="text-danger">End Date is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-8">
                    <label>Courier</label>
                    <select name="courier" class="{{ $errors->has('courier') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="">---- Select Courier ----</option>
                        <option value="All">All</option>
                        @foreach ($courier as $element)
                        <option value="{{ $element->company_name }}">{{ $element->company_name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('courier'))
                        <span class="text-danger">Courier is required</span>
                    @endif
                </div>
                <div class="form-group col-4">
                    <label>Type</label>
                    <select name="type" class="{{ $errors->has('type') ? ' form-control is-invalid' : 'form-control' }}">
                        @foreach ($courier as $element)
                        <option value="Air">Air</option>
                        <option value="Sea">Sea</option>
                        @endforeach
                    </select>
                    @if($errors->has('type'))
                        <span class="text-danger">Type is required</span>
                    @endif
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Generate</button>
        </form>
        @if (app('request')->input('start_date'))
        <hr>
            <div class="mt-4">
                <form method="post" action="{{ route('downloadManifest') }}">
                    <input type="hidden" name="start_date" value="{{app('request')->input('start_date')}}">
                    <input type="hidden" name="end_date" value="{{app('request')->input('end_date')}}">
                    <input type="hidden" name="courier" value="{{app('request')->input('courier')}}">
                    <button type="submit" class="btn btn-warning">Download</button>
                    <br><small>Download manifest for review</small>
                    {{ csrf_field() }}
                </form>
            </div>
            <hr>
            <h4 class="text-info">Summary</h4>
            <h5>Item Total: {{$itemCount}}</h5>
            <h5>Total Weight: {{ number_format($totalWeight, 2, '.', ',') }} lbs |  {{ number_format($totalWeight*0.453592, 2, '.', ',') }} kg</h5>
            <h5>Type: {{$type}}</h5>
            <div class="card shadow mb-4 mt-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Manifest {{app('request')->input('start_date')}} - {{app('request')->input('end_date')}}</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-bordered" width="100%" cellspacing="0" id="generate-manifest">
                        <thead>
                            <tr>
                                <th>Tracking Number</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Weight</th>
                                <th>Description</th>
                                <th>AWB</th>
                                <th>Shipper</th>
                                <th>Courier</th>
                                <th>Created</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $element)
                                <tr>
                                    <td>{{$element->tracking_number}}</td>
                                    <td>{{$element->first_name}}</td>
                                    <td>{{$element->last_name}}</td>
                                    <td>{{$element->weight}}</td>
                                    <td>{{$element->description}}</td>
                                    <td>{{$element->house_number}}</td>
                                    <td>{{$element->shipper}}</td>
                                    <td>{{$element->courier}}</td>
                                    <td>{{$element->created_at}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                {{ $data->appends(request()->input())->links("pagination::bootstrap-4") }}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        @endif
  </div>
@endsection