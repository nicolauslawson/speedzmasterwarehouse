@extends('templates.defaultadmin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">All Manifests</h1>
        <p class="mb-4">Table showing list of all manifests</p>
        @if(session()->has('delete'))
                <div class="alert alert-success" role="alert">
                    {{ session()->get('delete') }}
                </div>
        @endif
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">All Manifests</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0" id="view-manifest">
                <thead>
                <tr>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Courier</th>
                    <th>Weight</th>
                    <th>Items</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
            </table>
            </div>
        </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ url('/') }}/vendor/jquery/jquery.min.js"></script>

    <script>
        $(document).ready(function() {
                $('#view-manifest').DataTable({
                    processing: true,
                    responsive: false,
                    ajax: '{{ url('/') }}/manifest/datatable',
                    "lengthMenu": [[100, 300, 500], [100, 300, 500]],
                    columns: [
                        { data: 'start_date', name: 'start_date' },
                        { data: 'end_date', name: 'end_date' },
                        { data: 'courier', name: 'courier' },
                        { data: 'weight', name: 'weight' },
                        { data: 'item_count', name: 'item_count' },
                    ],
                    columnDefs: [ {
                        "targets": 5,
                        "data": "id",
                        "render": function ( data, type, row, meta ) {
                        return '<a class="btn btn-info" href="{{ url('/') }}/manifest/view?id='+data+'">View</a>';
                        }
                    } , {
                        "targets": 6,
                        "data": "id",
                        "render": function ( data, type, row, meta ) {
                        return '<a class="btn btn-danger" href="{{ url('/') }}/manifest/delete?id='+data+'">Delete</a>';
                        }
                    } ]
                });
            });
    </script>
@endsection