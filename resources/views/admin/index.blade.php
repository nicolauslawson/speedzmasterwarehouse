@extends('templates.defaultadmin')
@section('content')
        <div class="container-fluid">

          <!-- Content Row -->
          <div class="row">
            @if (Auth::user()->type == 1)
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Recent Items (Daily)</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">{{$recent}}</div>
                    </div>
                    <div class="col-auto">
                      <div class="nav-item dropdown no-arrow mx-1">
                          {{-- <i class="fas fa-sitemap fa-2x text-gray-300"></i> --}}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-dark text-uppercase mb-1">Unknown Requests</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">{{$recentAir}}</div>
                    </div>
                    <div class="col-auto">
                      {{-- <i class="fas fa-shopping-cart fa-2x text-gray-300"></i> --}}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Items</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{$recentSea}}</div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      {{-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> --}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endif
          </div>
          <div style="margin-top: 800px;"></div>
        </div>
@endsection
