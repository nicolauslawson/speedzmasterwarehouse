@extends('templates.defaultadmin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">All Users <span class="float-right"><a role="button" href="{{route('addUser')}}" class="btn btn-primary">Add User</a></span></h1>
        <p class="mb-4">Table showing list of all items</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">All Users</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="all-user">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>Mobile</th>
                            <th>Courier</th>
                            <th></th>
                        </tr>
                    </thead>
                     <tbody>
                         @foreach ($user as $element)
                            <tr>
                                <td>{{$element->first_name}}</td>
                                <td>{{$element->last_name}}</td>
                                <td>{{$element->email}}</td>
                                <td>{{$element->primary_address}}</td>
                                <td>{{$element->city}}</td>
                                <td>{{$element->mobile_cell}}</td>
                                <td>{{$element->courier}}</td>
                                <td><a role="button" class="btn btn-info text-white" href="{{route('viewSingleUser', ['id'=>$element->id])}}">View</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
@endsection
