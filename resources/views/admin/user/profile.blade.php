@extends('templates.defaultuser')
@section('content')
  {{-- <div class="container-fluid"> --}}
        <h1 class="h3 mb-0 text-gray-800">Profile</h1>
        <p class="mb-4">See account information below</p>
        <hr>
        @if(session()->has('message'))
            <div class="alert alert-success" role="alert">
                {{ session()->get('message') }}
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger" role="alert">
                {{ session()->get('error') }}
            </div>
        @endif
        <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Details for {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h6>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('profile') }}">
                    <div class="form-row">
                        <div class="form-group col-sm-12 col-md-2 col-lg-5">
                            <label>First Name</label>
                            <input type="text" value="{{ Auth::user()->first_name }}" name="first_name" class="{{ $errors->has('first_name') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                            @if($errors->has('first_name'))
                                <span class="text-danger">First Name is required</span>
                            @endif
                        </div>
                        <div class="form-group col-sm-12 col-md-5 col-lg-5">
                            <label>Last Name</label>
                            <input type="text" value="{{ Auth::user()->last_name }}" name="last_name" class="{{ $errors->has('last_name') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                            @if($errors->has('last_name'))
                                <span class="text-danger">Last Name is required</span>
                            @endif
                        </div>
                        <div class="form-group col-sm-12 col-md-5 col-lg-2">
                            <label>Mobile</label>
                            <input type="text" value="{{ Auth::user()->mobile_cell }}" name="mobile_cell" class="{{ $errors->has('mobile_cell') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                            @if($errors->has('mobile_cell'))
                                <span class="text-danger">Mobile is required</span>
                            @endif
                        </div>
                    </div>
                
                    <div class="form-row">
                        <div class="form-group col">
                            <label>Address</label>
                            <input type="text" value="{{ Auth::user()->primary_address }}" name="primary_address" class="{{ $errors->has('primary_address') ? ' form-control is-invalid' : 'form-control' }}">
                            @if($errors->has('primary_address'))
                                <span class="text-danger">Address is required</span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label>City</label>
                            <input type="text" value="{{ Auth::user()->city }}" name="city" class="{{ $errors->has('city') ? ' form-control is-invalid' : 'form-control' }}">
                            @if($errors->has('city'))
                                <span class="text-danger">City is required</span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label>Parish</label>
                            <input type="text" value="{{ Auth::user()->parish }}" name="parish" class="{{ $errors->has('parish') ? ' form-control is-invalid' : 'form-control' }}">
                            @if($errors->has('parish'))
                                <span class="text-danger">Parish is required</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-sm-12 col-md-6 col-lg-6">
                            <label>Email</label>
                            <input type="text" value="{{ Auth::user()->email }}" class="{{ $errors->has('email') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off" disabled readonly>
                        </div>
                        <div class="form-group col-sm-12 col-md-6 col-lg-6">
                            <label>Courier</label>
                            <input type="text" value="{{ Auth::user()->courier }}" class="{{ $errors->has('email') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off" disabled readonly>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success">Update</button>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
        <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Password Management</h6>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('password') }}">
                <div class="form-row">
                    <div class="form-group col">
                        <label>Current Password</label>
                        <input type="password" name="oldpassword" class="{{ $errors->has('oldpassword') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                        @if($errors->has('oldpassword'))
                            <span class="text-danger">First Name is required</span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label>New Password</label>
                        <input type="password" name="password" class="{{ $errors->has('password') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                        @if($errors->has('password'))
                            <span class="text-danger">Password is required</span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label>Confirm Password</label>
                        <input type="password" name="password_confirmation" class="{{ $errors->has('password_confirmation') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                        @if($errors->has('password_confirmation'))
                            <span class="text-danger">Confirm Password is required</span>
                        @endif
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Reset Password</button>
                {{ csrf_field() }}
            </form>
        </div>
@endsection