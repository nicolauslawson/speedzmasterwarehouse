@extends('templates.defaultadmin')
@section('content')
  <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add User</h1>
        </div>
        <hr>
        <form method="post" action="{{ route('addUser') }}">
            <div class="form-row">
                <div class="form-group col-6">
                    <label>First Name</label>
                    <input type="text" name="first_name" value="{{ old('first_name') }}" class="{{ $errors->has('first_name') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('first_name'))
                        <span class="text-danger">First Name is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>Last Name</label>
                    <input type="text" name="last_name" value="{{ old('last_name') }}" class="{{ $errors->has('last_name') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('last_name'))
                        <span class="text-danger">Last Name is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Email</label>
                    <input type="email" name="email" value="{{ old('email') }}" class="{{ $errors->has('email') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('email'))
                        <span class="text-danger">Email is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-6">
                    <label>Primary Address</label>
                    <input type="text" name="primary_address" value="{{ old('primary_address') }}" class="{{ $errors->has('primary_address') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('primary_address'))
                        <span class="text-danger">Primary Address is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>Secondary Address</label>
                    <input type="text" name="secondary_address" value="{{ old('secondary_address') }}" class="{{ $errors->has('secondary_address') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('secondary_address'))
                        <span class="text-danger">Secondary Address is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-sm">
                    <label>Country</label>
                    <input type="text" name="country" value="{{ old('country') }}" class="{{ $errors->has('country') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('country'))
                        <span class="text-danger">Country is required</span>
                    @endif
                </div>
                <div class="form-group col-sm">
                    <label>City</label>
                    <input type="text" name="city" value="{{ old('city') }}" class="{{ $errors->has('city') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('city'))
                        <span class="text-danger">City is required</span>
                    @endif
                </div>
                <div class="form-group col-sm">
                    <label>Parish</label>
                    <input type="text" name="parish" value="{{ old('parish') }}" class="{{ $errors->has('parish') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('parish'))
                        <span class="text-danger">Parish is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-6">
                    <label>Mobile #1</label>
                    <input type="text" name="mobile_1" value="{{ old('mobile_1') }}" class="{{ $errors->has('mobile_1') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('mobile_1'))
                        <span class="text-danger">Mobile #1 is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>Mobile #2</label>
                    <input type="text" name="mobile_2" value="{{ old('mobile_2') }}" class="{{ $errors->has('mobile_2') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('mobile_2'))
                        <span class="text-danger">Mobile #2 is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-6">
                    <label>Courier</label>
                    <select name="courier" value="{{ old('courier') }}" class="{{ $errors->has('courier') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="">---- Select Courier ----</option>
                        @foreach ($courier as $element)
                        <option value="{{ $element->company_name }}">{{ $element->company_name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('courier'))
                        <span class="text-danger">Courier is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>User Access Level</label>
                    <select name="access_level" value="{{ old('access_level') }}" class="{{ $errors->has('access_level') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="">---- Select User Type ----</option>
                        @foreach ($userType as $element)
                        <option value="{{ $element->type_id }}">{{ $element->type }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('access_level'))
                        <span class="text-danger">Access Level is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-6">
                    <label>Password</label>
                    <input type="password" name="password" class="{{ $errors->has('password') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('password'))
                        <span class="text-danger">Password is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>Confirm Password</label>
                    <input type="password" name="password_confirmation" class="{{ $errors->has('city') ? ' form-control is-invalid' : 'form-control' }}">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            {{ csrf_field() }}
        </form>
  </div>
@endsection
