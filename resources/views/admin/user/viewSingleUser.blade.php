@extends('templates.defaultadmin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">User Details</h1>
        <p class="mb-4">Section showing details for {{$user->first_name}}</p>
    </div>
    <hr>
    @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
    @endif
    @if(session()->has('block'))
            <div class="alert alert-danger">
                {{ session()->get('block') }}
            </div>
    @endif
    @if(session()->has('unblock'))
            <div class="alert alert-success">
                {{ session()->get('unblock') }}
            </div>
    @endif
    <div class="row">
        <div class="col-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Details for {{$user->first_name }} {{$user->last_name }}</h6>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('updateUser') }}">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label>First Name</label>
                            <input type="text" name="first_name" value="{{$user->first_name}}" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                            <label>Last Name</label>
                            <input type="text" name="last_name" value="{{$user->last_name}}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" value="{{$user->email}}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" name="address" value="{{$user->primary_address}}" class="form-control">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm">
                            <label>City</label>
                            <input type="text" name="city" value="{{$user->city}}" class="form-control">
                            </div>
                            <div class="form-group col-sm">
                            <label>Parish</label>
                            <input type="text" name="parish" value="{{$user->parish}}" class="form-control">
                            </div>
                            <div class="form-group col-sm">
                            <label>Country</label>
                            <input type="text" name="country" value="{{$user->country}}" class="form-control">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label>Mobile #1</label>
                            <input type="text" name="mobile_cell" value="{{$user->mobile_cell}}" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                            <label>Mobile #2</label>
                            <input type="text" name="mobile_other" value="{{$user->mobile_other}}" class="form-control">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label>User Access Level</label>
                                <select name="access_level" class="form-control">
                                    <option value="{{ $user->type }}">{{ $currentType->type }}</option>
                                    <option disabled>-----------------------------</option>
                                    @foreach ($userType as $element)
                                    <option value="{{ $element->type_id }}">{{ $element->type }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('access_level'))
                                    <span class="text-danger">Access Level is required</span>
                                @endif
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$user->id}}">
                        <button type="submit" class="btn btn-primary">Update</button>
                        
                        @if ($status->status == 'Blocked')
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#blockUserModal" onClick="modalClick('{{ $user->id }}', 'unblock')">Re-Activate</button>
                        @endif
                        @if ($status->status == 'Active')
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#blockUserModal" onClick="modalClick('{{ $user->id }}', 'block')">Deactivate</button>
                        @endif
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Additional Information</h6>
                </div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Created Date: <b>{{ \Carbon\Carbon::parse($user->created_at)->format('F d, Y')}}</b></label><br>
                            <label>Days a Member: <b>{{ \Carbon\Carbon::parse($user->created_at)->diffForHumans()}}</b></label><br>
                            <label>Assigned to: <b>{{$user->courier}}</b></label><br>
                            @if ($status->status == "Active")
                                <label>User Status: <b class="text-success">{{$status->status}}</b></label>
                            @elseif ($status->status == "Blocked")
                                <label>User Status: <b class="text-danger">{{$status->status}}</b></label>
                            @else
                                <label>User Status: <b class="text-warning">{{$status->status}}</b></label>
                            @endif
                        </div>
                        @if (Auth::user()->type == 1)
                            <span class="float-right">
                                <a role="button" href="{{ url('/') }}/admin/reset/password?id={{$user->id}}" class="btn-danger btn btn-sm">Reset Password</a><br>
                            <p class="text-danger">Default Password: warehouse#321</p>

                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="blockUserModal" tabindex="-1" role="dialog" aria-labelledby="blockUserModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="blockUserModalLabel">Block User!!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ route('blockUser') }}">
                <div class="modal-body">
                    Are you sure you want to <b>BLOCK</b> this user?
                    <input type="hidden" name="id" id="id" value=""/>
                    <input type="hidden" name="type" id="type" value=""/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
    {{-- <div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="deleteUserModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteUserModalLabel">Delete User!!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you want to <b>DELETE</b> this user?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
@section('scripts')
    <script src="{{ url('/') }}/vendor/jquery/jquery.min.js"></script>

    <script>
        function modalClick(id,type) {
            $(".modal-body #id").val(id);
            $(".modal-body #type").val(type);
        }
    </script>
@endsection
