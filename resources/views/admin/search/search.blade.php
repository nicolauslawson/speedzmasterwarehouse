@extends('templates.defaultadmin')
@section('content')
  <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">Search User</h1>
        <p class="mb-4">Select courier for user details</p>
        <form method="post" action="{{ route('searchuser') }}">
            <div class="form-row">
                <div class="form-group col-6">
                    <label for="">First Name</label>
                    <input type="text" name="fname" class="form-control">
                </div>
                <div class="form-group col-6">
                    <label for="">Last Name</label>
                    <input type="text" name="lname" class="form-control">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
            {{ csrf_field() }}
        </form>
  </div>
@endsection