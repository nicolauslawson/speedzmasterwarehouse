@extends('templates.defaultadmin')
@section('content')
  <div class="container-fluid">
      <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Generate Customer Invoice</h6>
        </div>
        <div class="card-body">
        <form method="post" action="{{ route('userInvoice') }}">
            <div class="form-row">
                <div class="form-group col-6">
                    <label>First Name</label>
                    <input type="text" name="first_name" value="{{ old('first_name') }}" class="{{ $errors->has('first_name') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('first_name'))
                        <span class="text-danger">First Name is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>Last Name</label>
                    <input type="text" name="last_name" value="{{ old('last_name') }}" class="{{ $errors->has('last_name') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('last_name'))
                        <span class="text-danger">Last Name is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-8">
                    <label>Type</label>
                    <select name="container" value="{{ old('container') }}" class="{{ $errors->has('container') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="">---- Select Container Type ----</option>
                        @foreach ($containers as $element)
                        <option value="{{ $element->id }}">{{ $element->name }} - ${{ $element->price }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('courier'))
                        <span class="text-danger">Courier is required</span>
                    @endif
                </div>
                <div class="form-group col-4">
                    <label>Quantity</label>
                    <select name="quantity" value="{{ old('quantity') }}" class="{{ $errors->has('quantity') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="0">-- Select Quantity --</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                    @if($errors->has('courier'))
                        <span class="text-danger">Courier is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-8">
                    <label>Shipping</label>
                    <select name="shipping_type" value="{{ old('shipping_type') }}" class="{{ $errors->has('shipping_type') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="">---- Select Shipping Type ----</option>
                        @foreach ($shipping as $element)
                        <option value="{{ $element->id }}">{{ $element->name }} - ${{ $element->price }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('courier'))
                        <span class="text-danger">Shipping is required</span>
                    @endif
                </div>
                <div class="form-group col-4">
                    <label>Quantity</label>
                    <select name="shipping_quantity" value="{{ old('shipping_quantity') }}" class="{{ $errors->has('shipping_quantity') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="0">-- Select Quantity --</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                    @if($errors->has('courier'))
                        <span class="text-danger">Courier is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row form-group">
                <div class="col">
                    <label>Freight</label>
                    <input type="text" id="freight" value="{{ old('freight') }}" name="freight" class="{{ $errors->has('freight') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                    @if($errors->has('freight'))
                        <span class="text-danger">Bags & Seals is required</span>
                    @endif
                </div>
                <div class="col">
                    <label>Shipping</label>
                    <input type="text" value="{{ old('shipping') }}" name="shipping" class="{{ $errors->has('shipping') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                    @if($errors->has('shipping'))
                        <span class="text-danger">AWB cost is required</span>
                    @endif
                </div>
            </div>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">Generate</button>
        </form>
  </div>
  </div>
@endsection

