<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Invoice for {{ $data->first_name }} {{ $data->last_name }}</title>
	<style>
		@font-face {
  font-family: SourceSansPro;
  src: url(SourceSansPro-Regular.ttf);
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #0087C3;
  text-decoration: none;
}

body {
  position: relative;
  width: 21cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #555555;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 14px; 
  font-family: SourceSansPro;
}

header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

#logo {
  float: left;
  margin-top: 8px;
}

#logo img {
  height: 70px;
}

#company {
  float: right;
  text-align: right;
}


#details {
  margin-bottom: 50px;
}

#client {
  padding-left: 6px;
  border-left: 6px solid #0087C3;
  float: left;
}

#client .to {
  color: #777777;
}

h2.name {
  font-size: 1.4em;
  font-weight: normal;
  margin: 0;
}

#invoice {
  float: right;
  text-align: right;
}

#invoice h1 {
  color: #0087C3;
  font-size: 2.4em;
  line-height: 1em;
  font-weight: normal;
  margin: 0  0 10px 0;
}

#invoice .date {
  font-size: 1.1em;
  color: #777777;
  margin-bottom: 10px;
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table th,
table td {
  padding: 20px;
  background: #EEEEEE;
  text-align: center;
  border-bottom: 1px solid #FFFFFF;
}

table th {
  white-space: nowrap;        
  font-weight: normal;
}

table td {
  text-align: right;
}

table td h3{
  color: #3498db;
  font-size: 1.2em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}

table .no {
  color: #FFFFFF;
  font-size: 1.2em;
  background: #3498db;
}

table .desc {
  text-align: left;
}

table .unit {
  background: #DDDDDD;
}

table .qty {
}

table .total {
  background: #3498db;
  color: #FFFFFF;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table tbody tr:last-child td {
  border: none;
}

table tfoot td {
  padding: 10px 20px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1.2em;
  white-space: nowrap; 
  border-top: 1px solid #AAAAAA; 
}

table tfoot tr:first-child td {
  border-top: none; 
}

table tfoot tr:last-child td {
  color: #3498db;
  font-size: 1.4em;
  border-top: 1px solid #3498db; 
}

table tfoot tr td:first-child {
  border: none;
}

#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

#notices .notice {
  font-size: 1.2em;
}

.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 10px 25px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}


	</style>
  </head>
  <body>
    <header class="clearfix">
      <div id="company">
        <img src="https://speedzmastershipping.com/img/pdflogo.png" alt="" height="35">
      </div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">INVOICE TO:</div>
          <h2 class="name">{{ $data->first_name }} {{ $data->last_name }}</h2>
        </div>
        <div id="invoice">
          <h1>INVOICE</h1>
          <div class="date">Invoice No.:</div>
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="qty">Type</th>
            <th class="unit">Unit Price</th>
            <th class="qty">Quantity</th>
            <th class="total">Total</th>
          </tr>
        </thead>
        <tbody>
            @if ($data->container)
            <tr>
              <td class="qty">{{ $data->container->name }}</td>
              <td class="unit">{{ $data->container->price }}</td>
              <td class="qty">{{ $data->container_quantity }}</td>
              <td class="total">{{ $data->container->price * $data->container_quantity }}</td>
            </tr>
            @endif
            @if ($data->shipping_val)
              <tr>
                <td class="qty">{{ $data->shipping_val->name }}</td>
                <td class="unit">{{ $data->shipping_val->price }}</td>
                <td class="qty">{{ $data->shipping_quantity }}</td>
                <td class="total">{{ $data->shipping_val->price * $data->shipping_quantity}}</td>
              </tr>
            @endif
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td colspan="1">SUBTOTAL</td>
            <td>$ {{ $subtotal }}</td>
          </tr>
          @if ($data->freight != 0)
          <tr>
            <td colspan="2"></td>
            <td colspan="1">Freight</td>
            <td>${{ $data->freight }}</td>
          </tr>
          @endif
          @if ($data->shipping != 0)
          <tr>
            <td colspan="2"></td>
            <td colspan="1">Shipping</td>
            <td>${{ $data->shipping }}</td>
          </tr>
          @endif
          <tr>
            <td colspan="2"></td>
            <td colspan="1">GRAND TOTAL</td>
            <td>$ {{ $total }}</td>
          </tr>
        </tfoot>
      </table>
      <p style="margin: 0; padding-top: 15px;">3453 NW 19th St, Fort Lauderdale, Florida, 33311</p>
      <p style="line-height: 0.4;"><b>Speedz Master Shipping</b></p>
      <p style="line-height: 0.4">Phone: 954-288-2480</p>
      <p style="line-height: 0.4"><b>warehouse.speedzmastershipping.com</b></p>
    </main>
    <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer>
  </body>
</html>