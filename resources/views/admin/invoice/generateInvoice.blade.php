@extends('templates.defaultadmin')
@section('content')
  <div class="container-fluid">
      <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Generate Invoice</h6>
        </div>
        <div class="card-body">
        <form method="get" action="{{ route('createInvoiceView') }}">
            <div class="form-row">
                <div class="form-group col-6">
                    <label>Start Date</label>
                    <input type="date" name="start_date" value="{{ old('start_date') }}" class="{{ $errors->has('start_date') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('start_date'))
                        <span class="text-danger">Start Date is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>End Date</label>
                    <input type="date" name="end_date" value="{{ old('end_date') }}" class="{{ $errors->has('end_date') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('end_date'))
                        <span class="text-danger">End Date is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12">
                    <label>Courier</label>
                    <select name="courier" value="{{ old('courier') }}" class="{{ $errors->has('courier') ? ' form-control is-invalid' : 'form-control' }}" id="courier_val" onchange="billing()">
                        <option value="">---- Select Courier ----</option>
                        @foreach ($courier as $element)
                        <option value="{{ $element->company_name }}">{{ $element->company_name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('courier'))
                        <span class="text-danger">Courier is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row form-group">
                <div class="col">
                    <label>Bags & Seals</label>
                    <input type="text" id="bags_cost" value="{{ old('bags_seals') }}" name="bags_seals" class="{{ $errors->has('bags_seals') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                    @if($errors->has('bags_seals'))
                        <span class="text-danger">Bags & Seals is required</span>
                    @endif
                </div>
                <div class="col">
                    <label>AWB Cost</label>
                    <input type="text" id="awb_cost" value="{{ old('handling') }}" name="handling" class="{{ $errors->has('handling') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                    @if($errors->has('handling'))
                        <span class="text-danger">AWB cost is required</span>
                    @endif
                </div>
                <div class="col">
                    <label>Delivery</label>
                    <input type="text" id="delivery_cost" value="{{ old('delivery_regular') }}" name="delivery_regular" class="{{ $errors->has('delivery_regular') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                    @if($errors->has('delivery_regular'))
                        <span class="text-danger">Delivery is required</span>
                    @endif
                </div>
            </div>
            <h4 class="mt-4">Range Rate</h4>
            <hr>
            <div class="form-row form-group">
                <div class="col">
                    <label>0-10 lbs</label>
                    <input type="text" id="tier1" name="teir1" class="{{ $errors->has('teir1') ? ' form-control is-invalid' : 'form-control' }}" value="1.50" autocomplete="off">
                    @if($errors->has('teir1'))
                        <span class="text-danger">Tier 1 is required</span>
                    @endif
                </div>
                <div class="col">
                    <label>11-20 lbs</label>
                    <input type="text" id="tier2" name="teir2" class="{{ $errors->has('teir2') ? ' form-control is-invalid' : 'form-control' }}" value="2.50" autocomplete="off">
                    @if($errors->has('teir2'))
                        <span class="text-danger">Tier 2 is required</span>
                    @endif
                </div>
                <div class="col">
                    <label>21-50 lbs</label>
                    <input type="text" id="tier3" name="teir3" class="{{ $errors->has('teir3') ? ' form-control is-invalid' : 'form-control' }}" value="6.00" autocomplete="off">
                    @if($errors->has('teir3'))
                        <span class="text-danger">Tier 3 is required</span>
                    @endif
                </div>
                <div class="col">
                    <label>51-100 lbs</label>
                    <input type="text" id="tier4" name="teir4" class="{{ $errors->has('teir4') ? ' form-control is-invalid' : 'form-control' }}" value="75.00" autocomplete="off">
                    @if($errors->has('teir4'))
                        <span class="text-danger">Tier 4 is required</span>
                    @endif
                </div>
                <div class="col">
                    <label>101-150 lbs</label>
                    <input type="text" id="tier5" name="teir5" class="{{ $errors->has('teir5') ? ' form-control is-invalid' : 'form-control' }}" value="90.00" autocomplete="off">
                    @if($errors->has('teir5'))
                        <span class="text-danger">Tier 5 is required</span>
                    @endif
                </div>
                <div class="col">
                    <label>151+ lbs</label>
                    <input type="text" id="tier6" name="teir6" class="{{ $errors->has('teir5') ? ' form-control is-invalid' : 'form-control' }}" value="100.00" autocomplete="off">
                    @if($errors->has('teir5'))
                        <span class="text-danger">Tier 6 is required</span>
                    @endif
                </div>
            </div>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">Generate</button>
        </form>
        <hr style="border: 1px solid black">
        <h2>Breakdown</h1>
        <h5>Item Count: <b>{{$items}}</b></h5>
        <h5>Bags & Seals: ${{app('request')->input('bags_seals')}}</h5>
        <h5>AWB Cost: ${{app('request')->input('handling')}}</h5>
        <h5>Delivery: ${{app('request')->input('delivery_regular')}}</h5>
        <h5>0-10 lb: <b>{{$teir1}}</b></h5>
        <h5>11-20 lb: <b>{{$teir2}}</b></h5>
        <h5>21-50 lb: <b>{{$teir3}}</b></h5>
        <h5>51-100 lb: <b>{{$teir4}}</b></h5>
        <h5>101-150 lb: <b>{{$teir5}}</b></h5>
        <h5>151+ lb: <b>{{$teir6}}</b></h5>
        <h5 class="mt-4 text-danger"><b>Total ${{$total}}</b></h5>
  </div>
  </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ url('/') }}/js/jquery.min.js"></script>
    <script>
        function billing() {
            var courier = document.getElementById("courier_val").value;
            $.ajax({
            url: "{{ url('/') }}/billing/details",
            type: "get", //send it through get method
            data: {
                courier: courier
            },
            success: function(response) {
                document.getElementById("tier1").value = response['data']['tier1'];
                document.getElementById("tier2").value = response['data']['tier2'];
                document.getElementById("tier3").value = response['data']['tier3'];
                document.getElementById("tier4").value = response['data']['tier4'];
                document.getElementById("tier5").value = response['data']['tier5'];
                document.getElementById("tier6").value = response['data']['tier6'];
                document.getElementById("delivery_cost").value = response['data']['delivery_cost'];
                document.getElementById("awb_cost").value = response['data']['awb_cost'];
                document.getElementById("bags_cost").value = response['data']['bags_cost'];
            },
            error: function(xhr) {
                alert("Please try again!")
            }
            });
        }
    </script>
@endsection
