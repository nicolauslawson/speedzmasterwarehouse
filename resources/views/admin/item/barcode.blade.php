@extends('templates.defaultadmin')
@section('content')
<div class="container-fluid">
    <h1 class="h3 mb-0 text-gray-800">Item Barcode</h1>
    <p class="mb-4">Print barcode for the package</p>
    <div id="myDivToPrint" class="myDivToPrint" style="margin-top: 0px; color: black;">
        <div>
            <p style="margin: 4; font-size: 40px">{{$newInvent->courier}} <span style="font-weight: bold; float: right;">{{$newInvent->weight}} lb</span></p>
            <hr style="border: 3px solid black;">
            <p style="padding-bottom: 15px; margin: 4; font-size: 40px">{{$newInvent->first_name}} {{$newInvent->last_name}}</p>
            @php
                echo DNS1D::getBarcodeSVG($newInvent->barcode, "C39",2.1,120);
            @endphp
            <p style="padding-top: 5px; margin: 4; font-size: 32px">{{$newInvent->barcode}}<span style="font-weight: bold; float: right;">Kingston</span></p>
        </div>
    </div>
    <button type="button" class="btn btn-success" onclick="myFunction()">Print</button>
    <a role="button" class="btn btn-dark" href="{{ route('addItem') }}">Add another item</a>
</div>
@endsection
@section('scripts')
  <script>
    function myFunction() {
        window.print();
    }
  </script>
@endsection
