@extends('templates.defaultadmin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">All Items</h1>
        <p class="mb-4">Table showing list of all items</p>

        @if(session()->has('message'))
            <div class="alert alert-success" role="alert">
                {{ session()->get('message') }}
            </div>
        @endif
        
        <h4 class="text-info">Summary</h4>
        <h5>Item Total: {{$itemCount}}</h5>
        <h5>Item Total (5 Mths): {{$itemCountMonth}}</h5>
        <h5>Total Weight: {{ number_format($totalWeight, 2, '.', ',') }} lbs |  {{ number_format($totalWeight*0.453592, 2, '.', ',') }} kg</h5>
        <div class="card shadow mb-4 mt-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">All Items: Table showing data for the last 5 months</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0" id="view-vehicle">
                <thead>
                    <tr>
                        <th>Tracking Number</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Description</th>
                        <th>Weight</th>
                        <th>House #</th>
                        <th>Shipper</th>
                        <th>Bag #</th>
                        <th>Courier</th>
                        <th>Created</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                    <tr>
                        <td>{{$item->tracking_number}}</td>
                        <td>{{$item->first_name}}</td>
                        <td>{{$item->last_name}}</td>
                        <td>{{$item->description}}</td>
                        <td>{{$item->weight}}</td>
                        <td>{{$item->house_number}}</td>
                        <td>{{$item->shipper}}</td>
                        <td>{{$item->bag_number}}</td>
                        <td>{{$item->courier}}</td>
                        <td>{{$item->created_at}}</td>
                        <td><a class="btn btn-danger btn-sm" href="{{ url('/') }}/admin/query/delete?house_number={{$item->house_number}}">Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    {{ $items->appends(request()->input())->links("pagination::bootstrap-4") }}
                </ul>
            </nav>
            </div>
        </div>
        </div>
    </div>
@endsection
