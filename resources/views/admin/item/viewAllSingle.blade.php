@extends('templates.defaultadmin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">All Single Items</h1>
        <p class="mb-4">Table showing list of all single items</p>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">All Single Items</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0" id="view-single">
                <thead>
                <tr>
                    <th>Tracking Number</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Description</th>
                    <th>Weight</th>
                    <th>House #</th>
                    <th>Shipper</th>
                    <th>Bag #</th>
                    <th>Courier</th>
                    <th>Created</th>
                    <th></th>
                </tr>
                </thead>
            </table>
            </div>
        </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ url('/') }}/vendor/jquery/jquery.min.js"></script>

    <script>
        $(document).ready(function() {
                $('#view-single').DataTable({
                    processing: true,
                    responsive: false,
                    ajax: '{{ url('/') }}/single/item/datatable',
                    "lengthMenu": [[100, 300, 500], [100, 300, 500]],
                    columns: [
                        { data: 'tracking_number', name: 'tracking_number' },
                        { data: 'first_name', name: 'first_name' },
                        { data: 'last_name', name: 'last_name' },
                        { data: 'description', name: 'description' },
                        { data: 'weight', name: 'weight' },
                        { data: 'house_number', name: 'house_number' },
                        { data: 'shipper', name: 'shipper' },
                        { data: 'bag_number', name: 'bag_number' },
                        { data: 'courier', name: 'courier' },
                        { data: 'created_at', name: 'created_at' },
                    ],
                    columnDefs: [ {
                        "targets": 10,
                        "data": "barcode",
                        "render": function ( data, type, row, meta ) {
                        return '<a class="btn btn-info" href="{{ url('/') }}/query/?barcode='+data+'">View</a>';
                        }
                    } ]
                });
            });
    </script>
@endsection