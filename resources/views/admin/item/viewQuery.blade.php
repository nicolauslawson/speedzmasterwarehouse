@extends('templates.defaultadmin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">Query Item</h1>
        <p class="mb-4">Scan item barcode for more information</p>
        @if(!empty($errors->first()))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first() }}
            </div>
        @endif
        <form method="get" action="{{ route('query') }}">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Scan Item</label>
                    <input type="text" name="barcode" class="form-control" autocomplete="off" autofocus>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
