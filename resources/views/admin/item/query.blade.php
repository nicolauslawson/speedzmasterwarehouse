@extends('templates.defaultadmin')
@section('content')
  <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">Query Item</h1>
        <p class="mb-4">Enter information located on the package</p>
        <form method="get" action="{{ route('reprint') }}">
            <input type="hidden" name="barcode" value="{{$item->barcode}}">
            <button type="submit" class="btn btn-warning">Reprint</button>
        </form>
        <form method="post" action="{{ route('query') }}">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Tracking Number</label>
                    <input type="text" name="tracking_number" value="{{$item->tracking_number}}" class="{{ $errors->has('tracking_number') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('tracking_number'))
                        <span class="text-danger">Tracking Number is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-6 col-sm-3">
                    <label>Prefix</label>
                    <input type="text" name="prefix" value="{{$item->prefix}}" class="{{ $errors->has('prefix') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('prefix'))
                        <span class="text-danger">Prefix is required</span>
                    @endif
                </div>
                <div class="form-group col">
                    <label>First Name</label>
                    <input type="text" name="first_name" value="{{$item->first_name}}" class="{{ $errors->has('first_name') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('first_name'))
                        <span class="text-danger">First Name is required</span>
                    @endif
                </div>
                <div class="form-group col">
                    <label>Last Name</label>
                    <input type="text" name="last_name" value="{{$item->last_name}}" class="{{ $errors->has('last_name') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('last_name'))
                        <span class="text-danger">Last Name is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col">
                    <label>Description</label>
                    <input type="text" name="description" value="{{$item->description}}" class="{{ $errors->has('description') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('description'))
                        <span class="text-danger">Description is required</span>
                    @endif
                </div>
                <div class="form-group col">
                    <label>Package Type</label>
                    <input type="text" name="package_type" value="{{$item->package_type}}" class="{{ $errors->has('package_type') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('package_type'))
                        <span class="text-danger">Package Type is required</span>
                    @endif
                </div>
                <div class="form-group col">
                    <label>Weight</label>
                    <input type="text" name="weight" value="{{$item->weight}}" class="{{ $errors->has('weight') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('weight'))
                        <span class="text-danger">Weight is required</span>
                    @endif
                </div>
                <div class="form-group col">
                    <label>Shipper</label>
                    <input type="text" name="shipper" value="{{$item->shipper}}" class="{{ $errors->has('shipper') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('shipper'))
                        <span class="text-danger">Shipper is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-6">
                    <label>Type</label>
                    <select name="type" class="form-control">
                        <option value="{{ $item->type }}">{{ $item->type }}</option>
                        <option disabled>-----------------------</option>
                        <option value="Regular">Regular</option>
                        <option value="Single">Single</option>
                    </select>
                </div>
                <div class="form-group col-6">
                    <label>Special Note</label>
                    <input type="text" name="note" value="{{$item->note}}" class="{{ $errors->has('note') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('note'))
                        <span class="text-danger">Special Note is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-6">
                    <label>Courier</label>
                    <input type="text" name="old_courier" value="{{$item->courier}}" class="{{ $errors->has('courier') ? ' form-control is-invalid' : 'form-control' }}" readonly>
                    @if($errors->has('courier'))
                        <span class="text-danger">Courier is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>House Number</label>
                    <input type="text" name="house_number" id="house_number_val" value="{{$item->house_number}}" class="{{ $errors->has('house_number') ? ' form-control is-invalid' : 'form-control' }}" readonly>
                </div>
            </div>
            @if (Auth::user()->type == 1)
            <hr>
             <div class="form-row">
                <div class="form-group col-3">
                    <label class="text-danger">Created By</label>
                    <input type="text" value="{{$item->created_by}}" class="form-control text-danger" readonly>
                </div>
                <div class="form-group col-3">
                    <label class="text-danger">Created Time</label>
                    <input type="text" value="{{$item->created_at}}" class="form-control text-danger" readonly>
                </div>
                @if ($item->type == 'Single')
                    <div class="form-group col-3">
                        <label class="text-danger">Single Item Number</label>
                        <input type="text" value="{{$item->barcode}}" class="form-control text-danger" readonly>
                    </div>
                @endif
                <div class="form-group col-3">
                    <label class="text-danger">Bag Number</label>
                    <input type="text" value="{{$item->bag_number}}" class="form-control text-danger" readonly>
                </div>
            </div>
            @endif
            <hr>
            <h3 class="text-danger">Update Information</h3>
            <div class="alert alert-info" role="alert">
                Changeing the courier will reset the house number and provide a new barcode
            </div>
            <div class="form-row">
                <div class="form-group col-12">
                    <label>Update Courier</label>
                    <select name="courier" class="{{ $errors->has('courier') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="">---- Select Courier ----</option>
                        @foreach ($courier as $element)
                        <option value="{{ $element->company_name }}">{{ $element->company_name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('courier'))
                        <span class="text-danger">Courier is required</span>
                    @endif
                </div>
            </div>
            <input type="text" name="barcode" class="form-control" value="{{ $item->barcode }}" hidden="true">
            <input type="text" name="id" class="form-control" value="{{ $item->id }}" hidden="true">
            <button type="submit" class="btn btn-primary">Update Item</button>
            @if (Auth::user()->type == 1)
                <button type="button" onclick="deleteItem()" class="btn btn-danger float-right">Permanently DELETE</button>
            @endif
            {{ csrf_field() }}
        </form>
  </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ url('/') }}/js/jquery.min.js"></script>
    <script>
        function deleteItem() {
            var house_number = document.getElementById("house_number_val").value;
            $.ajax({
            url: "{{ url('/') }}/admin/query/delete",
            type: "get", //send it through get method
            data: {
                house_number: house_number
            },
            success: function(response) {
                alert(response);
                window.location.href = "/admin";
            },
            error: function(xhr) {
                alert("Please try again!")
            }
            });
        }
    </script>
@endsection
