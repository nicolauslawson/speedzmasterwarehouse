@extends('templates.defaultadmin')
@section('content')
  <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">Search Item</h1>
        <p class="mb-4">Search for customer item</p>
        <hr>
        <form method="get" action="{{ route('searchItemAdminView') }}">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>First Name</label>
                    <input type="text" name="first_name" value="{{app('request')->input('first_name')}}"  class="{{ $errors->has('first_name') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                </div>
                <div class="form-group col-md-6">
                    <label>Last Name</label>
                    <input type="text" name="last_name" value="{{app('request')->input('last_name')}}"  class="{{ $errors->has('last_name') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Tracking Number</label>
                    <input type="text" name="tracking_number" value="{{app('request')->input('tracking_number')}}"  class="{{ $errors->has('tracking_number') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Start Date</label>
                    <input type="date" name="date_from" value="{{app('request')->input('date_from')}}" class="{{ $errors->has('date_from') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                </div>
                <div class="form-group col-md-6">
                    <label>End Date</label>
                    <input type="date" name="date_to" value="{{app('request')->input('date_to')}}"  class="{{ $errors->has('date_to') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
        <div class="card shadow mb-4 mt-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Search Results</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="pack-bag">
                    <thead>
                        <tr>
                            <th>Tracking Number</th>
                            <th>Mailbox</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Weight</th>
                            <th>AWB</th>
                            <th>Shipper</th>
                            <th>Bag</th>
                            <th>Courier</th>
                            <th>Package Type</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $item)
                        <tr>
                            <td>{{$item->tracking_number}}</td>
                            <td>{{$item->prefix}}</td>
                            <td>{{$item->first_name}} {{$item->last_name}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->weight}}</td>
                            <td>{{$item->house_number}}</td>
                            <td>{{$item->shipper}}</td>
                            <td>{{$item->bag_number}}</td>
                            <td>{{$item->courier}}</td>
                            <td>{{$item->package_type}}</td>
                            <td>{{$item->created_at}}</td>
                            <td>
                                <a role="button" class="btn btn-warning text-white" href="{{route('reprint', ['barcode'=>$item->house_number])}}"><i class="fas fa-redo"></i></a>
                                <a role="button" class="btn btn-danger text-white" href="{{ url('/') }}/admin/query/delete?house_number={{$item->house_number}}"><i class="fas fa-trash-alt"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        {{ $data->appends(request()->input())->links("pagination::bootstrap-4") }}
                    </ul>
                </nav>
                </div>
            </div>
        </div>
  </div>
@endsection