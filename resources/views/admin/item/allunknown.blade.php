@extends('templates.defaultadmin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">All Unknown</h1>
        <div class="card shadow mb-4 mt-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">All Unknown Items</h6>
            </div>
            <div class="card-body">
               
                <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="pack-bag">
                    <thead>
                    <tr>
                        <th>Tracking Number</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>House #</th>
                        <th>Weight</th>
                        <th>Shipper</th>
                        <th></th>
                    </tr>
                    </thead>
                    @foreach ($items as $element)
                        <tr>
                            <td>
                                {{$element->tracking_number}}<br>
                                @foreach ($element['data'] as $item)
                                    <span class="text-danger" style="font-size: 14px;">{{$item['courier']}}: {{$item['tracking_number']}}</span><br>
                                @endforeach
                            </td>
                            <td>{{$element->first_name}}</td>
                            <td>{{$element->last_name}}</td>
                            <td>{{$element->house_number}}</td>
                            <td>{{$element->weight}}</td>
                            <td>{{$element->shipper}}</td>
                            {{-- <td>{{$element->request($element->id)}}</td> --}}
                            <td><a role="button" class="btn btn-sm btn-info text-white" href="{{route('query', ['barcode'=>$element->house_number])}}">Query</a></td>
                        </tr>
                    @endforeach
                </table>
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        {{ $items->appends(request()->input())->links("pagination::bootstrap-4") }}
                    </ul>
                </nav>
                </div>
            </div>
        </div>
    </div>
@endsection