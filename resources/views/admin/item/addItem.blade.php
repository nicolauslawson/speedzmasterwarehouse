@extends('templates.defaultadmin')
@section('content')
  <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">Add Item</h1>
        <p class="mb-4">Enter information located on the package</p>
        <form method="post" action="{{ route('addItem') }}" target="_blank">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Tracking Number</label>
                    <div class="input-group">
                        <input type="text" value="{{ old('tracking_number') }}" name="tracking_number" id="tracking_number_val" onchange="checkTracking()" class="{{ $errors->has('tracking_number') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                        <button class="btn btn-info text-white" type="button" onclick="generateTracking()">Generate Tracking #</button>
                    </div>
                    @if($errors->has('tracking_number'))
                        <span class="text-danger">Tracking Number is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-6 col-sm-3">
                    <label>Mailbox</label>
                    <input type="text" value="{{ old('prefix') }}" name="prefix" class="{{ $errors->has('prefix') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                    @if($errors->has('prefix'))
                        <span class="text-danger">Prefix is required</span>
                    @endif
                </div>
                <div class="form-group col">
                    <label>First Name</label>
                    <input type="text" value="{{ old('first_name') }}" name="first_name" class="{{ $errors->has('first_name') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                    @if($errors->has('first_name'))
                        <span class="text-danger">First Name is required</span>
                    @endif
                </div>
                <div class="form-group col">
                    <label>Last Name</label>
                    <input type="text" value="{{ old('last_name') }}" name="last_name" class="{{ $errors->has('last_name') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                    @if($errors->has('last_name'))
                        <span class="text-danger">Last Name is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col">
                    <label>Description</label>
                    {{-- <input type="text" value="{{ old('description') }}" name="description" class="{{ $errors->has('description') ? ' form-control is-invalid' : 'form-control' }}"> --}}
                    <select name="description" class="form-control">
                        <option value="" disabled selected>-- Select Package Description --</option>
                        <option value="Merchandise">Merchandise</option>
                        <option value="Accessories">Accessories</option>
                        <option value="Clothing">Clothing</option>
                        <option value="Shoes">Shoes</option>
                        <option value="Barrel">Barrel</option>
                        <option value="Electronics">D Container</option>
                        <option value="Electronics">E Container</option>
                        <option value="Electronics">Pallet</option>
                        <option value="Crate">Crate</option>
                    </select>
                    @if($errors->has('description'))
                        <span class="text-danger">Description is required</span>
                    @endif
                </div>
                <div class="form-group col">
                    <label>Package Type</label>
                    <input type="text" value="{{ old('package_type') }}" name="package_type" class="{{ $errors->has('package_type') ? ' form-control is-invalid' : 'form-control' }}">
                    <small class="text-danger">*Add short description for the package (Yellow bag, Long box etc..)</small>
                    @if($errors->has('package_type'))
                        <span class="text-danger">Package Type is required</span>
                    @endif
                </div>
                <div class="form-group col">
                    <label>Weight</label>
                    <input type="text" value="{{ old('weight') }}" name="weight" class="{{ $errors->has('weight') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('weight'))
                        <span class="text-danger">Weight is required</span>
                    @endif
                </div>
                <div class="form-group col">
                    <label>Shipper</label>
                    <input type="text" value="{{ old('shipper') }}" name="shipper" class="{{ $errors->has('shipper') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('shipper'))
                        <span class="text-danger">Shipper is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-6">
                    <label>Item Type</label>
                    <select id="item_" name="item_type" value="{{ old('item_type') }}" class="{{ $errors->has('item_type') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="Regular">Regular</option>
                    </select>
                    <small class="text-danger">* Regular items are default. Only if single item you need to change this</small>
                    @if($errors->has('item_type'))
                        <span class="text-danger">Item Type is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>Special Note</label>
                    <input type="text" value="{{ old('note') }}" name="note" class="{{ $errors->has('note') ? ' form-control is-invalid' : 'form-control' }}">
                    @if($errors->has('note'))
                        <span class="text-danger">Special Note is required</span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-6">
                    <label>Courier</label>
                    <select name="courier" value="{{ old('courier') }}" class="{{ $errors->has('courier') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="">---- Select Courier ----</option>
                        @foreach ($courier as $element)
                        <option value="{{ $element->company_name }}">{{ $element->company_name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('courier'))
                        <span class="text-danger">Courier is required</span>
                    @endif
                </div>
                <div class="form-group col-6">
                    <label>Shipping Method</label>
                    <select name="shipping_type" value="{{ old('shipping_type') }}" class="{{ $errors->has('shipping_type') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="Air" selected>Air</option>
                        <option value="Sea">Sea</option>
                    </select>
                    @if($errors->has('shipping_type'))
                        <span class="text-danger">Shipping Type is required</span>
                    @endif
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Add Item</button>
            <a role="button" href="{{route('addItem')}}" class="btn btn-warning text-white">Add Another Item</a>
            {{ csrf_field() }}
        </form>
  </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ url('/') }}/js/jquery.min.js"></script>
    <script>
        function checkTracking() {
            var tracking_number = document.getElementById("tracking_number_val").value;
            $.ajax({
            url: "{{ url('/') }}/tracking/check",
            type: "get", //send it through get method
            data: {
                tracking_number: tracking_number
            },
            success: function(response) {
                if (response.status == 'true') {
                    var status = "Tracking Number already added!!.";
                    alert(status);
                    document.getElementById("tracking_number_val").value = "";
                    window.location.href = "/query?barcode=" + response.barcode;
                }
            },
            error: function(xhr) {
                alert("Please try again!")
            }
            });
        }
        $("#item_").change(function () {
            var selected_option = $('#item_').val();
            if (selected_option === 'Single') {
                $('#subcourier').show();
            }
            if (selected_option != 'Single') {
                $('#subcourier').hide();
                $('#sub_courier').value = null;
            }
        })
        function generateTracking() {
            str1 = 'notrk'
            document.getElementById("tracking_number_val").value = str1.concat(Math.random().toString(36).substr(2, 25));
        }
    </script>
@endsection
