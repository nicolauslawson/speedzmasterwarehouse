@extends('templates.defaultadmin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">Pack Bag</h1>
        <p class="">Scan items to bag</p>
        <h5 class="">Bag Number: <span class="text-danger">{{app('request')->input('bag_code')}}</span></h5>
        <h5 class="">Courier: <span class="text-danger">{{$item->courier}}</span></h5>
        <h5 class="">Weight: <span class="text-danger">{{ number_format($weight, 2, '.', ',') }} lbs |  {{ number_format($weight*0.453592, 2, '.', ',') }} kg</span></h5>
        @if(!empty($errors->first()))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first() }}
                <input type="hidden" id="alert" value="true">
            </div>
        @endif
        @if(session()->has('exist'))
                <div class="alert alert-danger" role="alert">
                    {{ session()->get('exist') }}
                </div>
        @endif
        <form method="post" action="{{ route('packBagView') }}">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Scan Item</label>
                    <input type="text" name="item_barcode" class="form-control" autocomplete="off" autofocus>
                    <input type="hidden" name="bag_number" value="{{app('request')->input('bag_code')}}" class="form-control">
                </div>
            </div>
            {{ csrf_field() }}
            {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
        </form>
        <div class="card shadow mb-4 mt-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Bag {{app('request')->input('bag_code')}}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="pack-bag">
                    <thead>
                    <tr>
                        <th>Tracking Number</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Weight</th>
                        <th>AWB</th>
                        <th>Shipper</th>
                        <th>Courier</th>
                        <th></th>
                    </tr>
                    </thead>
                    @foreach ($bag as $element)
                        <tr>
                            <td>{{$element->tracking_number}}</td>
                            <td>{{$element->first_name}}</td>
                            <td>{{$element->last_name}}</td>
                            <td>{{$element->weight}}</td>
                            <td>{{$element->house_number}}</td>
                            <td>{{$element->shipper}}</td>
                            <td>{{$element->courier}}</td>
                            <td><a role="button" class="btn btn-danger text-white" href="{{route('packDelete', ['id'=>$element->house_number])}}">Delete</a></td>
                        </tr>
                    @endforeach
                </table>
                </div>
            </div>
        </div>
    </div>
@endsection