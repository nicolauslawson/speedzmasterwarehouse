@extends('templates.defaultadmin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">Pack Bag</h1>
        <p class="mb-4">Scan bag to get started</p>
        @if(!empty($errors->first()))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first() }}
            </div>
        @endif
        <form method="get" action="{{ route('packBagSubmit') }}">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Scan Bag</label>
                    <input type="text" name="bag_code" class="form-control" autocomplete="off" autofocus>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection