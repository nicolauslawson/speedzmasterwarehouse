@extends('templates.defaultadmin')
@section('content')
  <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">Create Bag</h1>
        <p class="mb-4">Select courier to create bag</p>
        <form method="post" action="{{ route('createBag') }}" target="_blank">
            <div class="form-row">
                <div class="form-group col-7">
                    <label>Courier</label>
                    <select name="courier" class="{{ $errors->has('courier') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="">---- Select Courier ----</option>
                        @foreach ($courier as $element)
                        <option value="{{ $element->company_name }}">{{ $element->company_name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('courier'))
                        <span class="text-danger">Courier is required</span>
                    @endif
                </div>
                <div class="form-group col-5">
                    <label>Type</label>
                    <select name="type" class="{{ $errors->has('type') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="Regular">Bag</option>
                        <option value="Single">Single</option>
                    </select>
                    @if($errors->has('type'))
                        <span class="text-danger">Type is required</span>
                    @endif
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Create Bag</button>
            {{ csrf_field() }}
        </form>
  </div>
@endsection