@extends('templates.defaultadmin')
@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">All Bags</h1>
        <p class="mb-4">List of all generated bags</p>
        <form>
            <div class="form-row">
                <div class="form-group col-4">
                    <label>Courier</label>
                    <select name="courier" value="{{ old('courier') }}" class="{{ $errors->has('courier') ? ' form-control is-invalid' : 'form-control' }}">
                        <option value="">---- Select Courier ----</option>
                        <option value="">All</option>
                        @foreach ($courier as $element)
                        <option value="{{ $element->id }}">{{ $element->company_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Start Date</label>
                    <input type="date" name="sd" value="{{ app('request')->input('sd') ? app('request')->input('sd') : '2021-01-01' }}" class="{{ $errors->has('sd') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                </div>
                <div class="form-group col-md-6">
                    <label>End Date</label>
                    <input type="date" name="ed" value="{{ app('request')->input('ed') ? app('request')->input('ed') : $date }}"  class="{{ $errors->has('ed') ? ' form-control is-invalid' : 'form-control' }}" autocomplete="off">
                </div>
            </div>
            <button class="btn btn-info">Generate</button>
        </form>
        Items: <span style="font-weight: bold; color: red">{{count($bag)}} </span> <br>
        Total Weight: <span style="font-weight: bold; color: red">{{$weight}} lbs</span> 
        <div class="card shadow mb-4 mt-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">All Bags</h6>
            </div>
            <div class="card-body">
               
                <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="pack-bag">
                    <thead>
                    <tr>
                        <th>Courier</th>
                        <th>Bag Number</th>
                        <th>Code</th>
                        <th>Items</th>
                        <th>Weight (lb)</th>
                        <th>Created</th>
                        <th></th>
                    </tr>
                    </thead>
                    @foreach ($bag as $element)
                        <tr>
                            <td>{{$element->courier}}</td>
                            <td>{{$element->bag_number}}</td>
                            <td>{{$element->bag_code}}</td>
                            <td>{{$element->count($element->bag_number)}}</td>
                            <td>{{$element->weight($element->bag_number)}}</td>
                            <td>{{$element->created_at}}</td>
                            <td><a role="button" class="btn btn-info text-white" href="{{route('checkBag', ['bag_code'=>$element->bag_number])}}">View</a> <a role="button" class="btn btn-warning text-white ml-2" href="{{route('reprintbag', ['id'=>$element->bag_number])}}">Re-Print</a></td>
                        </tr>
                    @endforeach
                </table>
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        {{ $bag->appends(request()->input())->links("pagination::bootstrap-4") }}
                    </ul>
                </nav>
                </div>
            </div>
        </div>
    </div>
@endsection