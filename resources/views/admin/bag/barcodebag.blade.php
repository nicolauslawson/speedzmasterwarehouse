@extends('templates.defaultadmin')
@section('content')
  <div class="container-fluid">
        <h1 class="h3 mb-0 text-gray-800">Bag Barcode</h1>
        <p class="mb-4">Print barcode for the bag</p>
            <div class="text-center myDivToPrint" style="margin-top: 10px; color: black;">
                <div>
                    <p style="margin: 4; font-size: 18px">{{$bag->courier}} </p>
                    <p style="margin: 4; font-size: 18px">{{$bag->bag_number}} </p>
                    @php
                        echo DNS1D::getBarcodeSVG($bag->bag_number, "C39",1,30);
                    @endphp
                </div>
            </div>
            <button type="button" class="btn btn-success" onclick="myFunction()">Print</button>
            <a role="button" class="btn btn-dark" href="{{ route('createBag') }}">Add another bag</a>
            <a role="button" class="btn btn-warning" href="{{ route('packBagSubmit', ['bag_code' => $bag->bag_number]) }}">Pack Bag</a>
  </div>
@endsection
@section('scripts')
  <script>
    function myFunction() {
        window.print();
    }
  </script>
@endsection
